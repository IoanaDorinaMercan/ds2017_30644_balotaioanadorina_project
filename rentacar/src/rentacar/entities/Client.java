package rentacar.entities;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import org.hibernate.annotations.Proxy;

@Entity
@Table(name = "clienti")
@Proxy(lazy = false)
public class Client implements java.io.Serializable{

	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private String nume;
	private String prenme;
	private String cnp;
	private String adresa;
	private Integer varsta;
	private String email;
	private String telefon;
	private String categoriePermis;
	private String vechimePermis;
	private String observatii;
	private CodActivare codActivare;
	
	public Client() {}

	public Client(Integer id, String nume, String prenme, String cnp, String adresa, Integer varsta, String email,
			String telefon, String categoriePermis, String vechimePermis, String observatii) {
		super();
		this.id = id;
		this.nume = nume;
		this.prenme = prenme;
		this.cnp = cnp;
		this.adresa = adresa;
		this.varsta = varsta;
		this.email = email;
		this.telefon = telefon;
		this.categoriePermis = categoriePermis;
		this.vechimePermis = vechimePermis;
		this.observatii = observatii;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "nume",nullable = false,length=100)
	public String getNume() {
		return nume;
	}

	public void setNume(String nume) {
		this.nume = nume;
	}

	@Column(name = "prenume",nullable = false,length=100)
	public String getPrenme() {
		return prenme;
	}

	public void setPrenme(String prenme) {
		this.prenme = prenme;
	}

	@Column(name = "cnp",unique=true,nullable = false,length=13)
	public String getCnp() {
		return cnp;
	}

	public void setCnp(String cnp) {
		this.cnp = cnp;
	}

	@Column(name = "adresa",nullable = false,length=100)
	public String getAdresa() {
		return adresa;
	}

	public void setAdresa(String adresa) {
		this.adresa = adresa;
	}

	@Column(name = "varsta",nullable = false,length=100)
	public Integer getVarsta() {
		return varsta;
	}

	public void setVarsta(Integer varsta) {
		this.varsta = varsta;
	}
	@Column(name = "email",unique=true,nullable = false,length=100)
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	@Column(name = "telefon",unique=true,nullable = false,length=100)
	public String getTelefon() {
		return telefon;
	}

	public void setTelefon(String telefon) {
		this.telefon = telefon;
	}

	@Column(name = "categorie_permis",nullable = false,length=100)
	public String getCategoriePermis() {
		return categoriePermis;
	}

	public void setCategoriePermis(String categoriePermis) {
		this.categoriePermis = categoriePermis;
	}

	@Column(name = "vechime_permis",nullable = false,length=100)
	public String getVechimePermis() {
		return vechimePermis;
	}

	public void setVechimePermis(String vechimePermis) {
		this.vechimePermis = vechimePermis;
	}

	@Column(name = "observatii",nullable = true,length=100)
	public String getObservatii() {
		return observatii;
	}

	public void setObservatii(String observatii) {
		this.observatii = observatii;
	}

	@OneToOne(fetch = FetchType.LAZY, mappedBy = "client", cascade = CascadeType.ALL)
	//@JsonIgnore
	public CodActivare getCodActivare() {
		return codActivare;
	}

	public void setCodActivare(CodActivare codActivare) {
		this.codActivare = codActivare;
	}

	
	
	
}
