package rentacar.entities;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "rating")
public class Rating implements java.io.Serializable{

	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private Masina masina;
	private Client client;
	private Float nota;
	private String comentariu;
	
	public Rating() {}

	public Rating(Integer id, Masina masina, Client client, Float nota, String comentariu) {
		super();
		this.id = id;
		this.masina = masina;
		this.client = client;
		this.nota = nota;
		this.comentariu = comentariu;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_masina")
	@JsonIgnore
	public Masina getMasina() {
		return masina;
	}

	public void setMasina(Masina masina) {
		this.masina = masina;
	}

	@OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_client")
	@JsonIgnore
	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	@Column(name = "nota",  nullable = false)
	public Float getNota() {
		return nota;
	}

	public void setNota(Float nota) {
		this.nota = nota;
	}

	@Column(name = "comentariu",nullable = false)
	public String getComentariu() {
		return comentariu;
	}

	public void setComentariu(String comentariu) {
		this.comentariu = comentariu;
	}
	
	
	
}
