package rentacar.entities;

import static javax.persistence.GenerationType.IDENTITY;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Proxy;

@Entity
@Table(name = "inchirieri_masini")
@Proxy(lazy = false)
public class InchiriereMasina implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;
	private Integer id;
	private Masina masina;
	private Client client;
	@Temporal(TemporalType.TIMESTAMP)
	private Timestamp dataStart;
	@Temporal(TemporalType.TIMESTAMP)
	private Timestamp dataStop;
	private Locatie locatieRidicare;
	private Locatie locatiePredare;
	
	public InchiriereMasina() {}

	public InchiriereMasina(Integer id, Masina masina, Client client, Timestamp dataStart, Timestamp dataStop,
			Locatie locatieRidicare, Locatie locatiePredare) {
		super();
		this.id = id;
		this.masina = masina;
		this.client = client;
		this.dataStart = dataStart;
		this.dataStop = dataStop;
		this.locatieRidicare = locatieRidicare;
		this.locatiePredare = locatiePredare;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	
	@OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_masina")
	public Masina getMasina() {
		return masina;
	}

	public void setMasina(Masina masina) {
		this.masina = masina;
	}

	@OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_client")
	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	@Column(name = "data_start",  nullable = false)
	public Timestamp getDataStart() {
		return dataStart;
	}

	public void setDataStart(Timestamp dataStart) {
		this.dataStart = dataStart;
	}


	@Column(name = "data_stop",  nullable = false)
	public Timestamp getDataStop() {
		return dataStop;
	}

	public void setDataStop(Timestamp dataStop) {
		this.dataStop = dataStop;
	}

	@OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_locatie_ridicare")
	public Locatie getLocatieRidicare() {
		return locatieRidicare;
	}

	public void setLocatieRidicare(Locatie locatieRidicare) {
		this.locatieRidicare = locatieRidicare;
	}

	@OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_locatie_predare")
	public Locatie getLocatiePredare() {
		return locatiePredare;
	}

	public void setLocatiePredare(Locatie locatiePredare) {
		this.locatiePredare = locatiePredare;
	}
	
	
	

}
