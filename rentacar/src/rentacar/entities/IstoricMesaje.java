package rentacar.entities;

import static javax.persistence.GenerationType.IDENTITY;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Proxy;


@Entity
@Table(name = "istoric_mesaje")
@Proxy(lazy = false)
public class IstoricMesaje implements java.io.Serializable{

	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private Utilizator admin;
	private String mesaj;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Timestamp dataTrimitere;
	private Integer nrPersoane;

	public IstoricMesaje() {}

	public IstoricMesaje(Integer id, Utilizator admin, String mesaj, Timestamp dataTrimitere, Integer nrPersoane) {
		super();
		this.id = id;
		this.admin = admin;
		this.mesaj = mesaj;
		this.dataTrimitere = dataTrimitere;
		this.nrPersoane = nrPersoane;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_admin")
	public Utilizator getAdmin() {
		return admin;
	}

	public void setAdmin(Utilizator admin) {
		this.admin = admin;
	}

	@Column(name = "mesaj", nullable = false,length=1000)
	public String getMesaj() {
		return mesaj;
	}

	public void setMesaj(String mesaj) {
		this.mesaj = mesaj;
	}

	@Column(name = "data_trimitere",nullable = false)
	public Timestamp getDataTrimitere() {
		return dataTrimitere;
	}

	public void setDataTrimitere(Timestamp dataTrimitere) {
		this.dataTrimitere = dataTrimitere;
	}

	@Column(name = "numar_persoane", nullable = false)
	public Integer getNrPersoane() {
		return nrPersoane;
	}

	public void setNrPersoane(Integer nrPersoane) {
		this.nrPersoane = nrPersoane;
	}
	
}
