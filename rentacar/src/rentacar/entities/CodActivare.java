package rentacar.entities;

import static javax.persistence.GenerationType.IDENTITY;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "coduri_activare")
public class CodActivare implements java.io.Serializable{
	
	private static final long serialVersionUID = 1L;
	private Integer id;
	private Client client;
	private String cod;
	private Boolean activat;
	private Date dataActivarii;
	
	public CodActivare() {}

	public CodActivare(Integer id, Client client, String cod, Boolean activat, Date dataActivarii) {
		super();
		this.id = id;
		this.client = client;
		this.cod = cod;
		this.activat = activat;
		this.dataActivarii = dataActivarii;
	}


	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_client",nullable = false)
	@JsonIgnore
	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	@Column(name = "cod", unique = true, nullable = false,length=100)
	public String getCod() {
		return cod;
	}

	public void setCod(String cod) {
		this.cod = cod;
	}

	@Column(name = "activat",nullable = false)
	public Boolean getActivat() {
		return activat;
	}

	public void setActivat(Boolean activat) {
		this.activat = activat;
	}
	@Column(name = "data_activarii", nullable = false)
	public Date getDataActivarii() {
		return dataActivarii;
	}

	public void setDataActivarii(Date dataActivarii) {
		this.dataActivarii = dataActivarii;
	}
	
	
}
