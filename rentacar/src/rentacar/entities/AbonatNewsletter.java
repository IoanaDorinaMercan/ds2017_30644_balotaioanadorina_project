package rentacar.entities;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "abonati_newsletter")
public class AbonatNewsletter implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private String nume;
	private String email;
	
	public AbonatNewsletter() {}

	public AbonatNewsletter(Integer id, String nume, String email) {
		super();
		this.id = id;
		this.nume = nume;
		this.email = email;
	}


	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "nume", nullable = false,length=100)
	public String getNume() {
		return nume;
	}

	public void setNume(String nume) {
		this.nume = nume;
	}

	@Column(name = "email", nullable = false,length=100)
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	
}
