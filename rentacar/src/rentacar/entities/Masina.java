package rentacar.entities;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Proxy;

@Entity
@Table(name = "masini")
@Proxy(lazy = false)
public class Masina implements java.io.Serializable{

	private static final long serialVersionUID = 1L;
	private Integer id;
	private String marca;
	private String model;
	private Integer anFabricatie;
	private String tipCombustibil;
	private Float consumMediu;
	private Integer caiPutere;
	private String nrInmatriculare;
	private Integer nrLocuri;
	private Float pretInchiriere;
	private String statusMasina;
	private Integer timpMinimInchiriere;
	
	public Masina() {}

	public Masina(Integer id, String marca, String model, Integer anFabricatie, String tipCombustibil,
			Float consumMediu, Integer caiPutere, String nrInmatriculare, Integer nrLocuri, Float pretInchiriere,
			String statusMasina, Integer timpMinimInchiriere) {
		super();
		this.id = id;
		this.marca = marca;
		this.model = model;
		this.anFabricatie = anFabricatie;
		this.tipCombustibil = tipCombustibil;
		this.consumMediu = consumMediu;
		this.caiPutere = caiPutere;
		this.nrInmatriculare = nrInmatriculare;
		this.nrLocuri = nrLocuri;
		this.pretInchiriere = pretInchiriere;
		this.statusMasina = statusMasina;
		this.timpMinimInchiriere = timpMinimInchiriere;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "marca", nullable = false,length=100)
	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	@Column(name = "model", nullable = false,length=100)
	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	@Column(name = "an_fabricatie", nullable = false)
	public Integer getAnFabricatie() {
		return anFabricatie;
	}

	public void setAnFabricatie(Integer anFabricatie) {
		this.anFabricatie = anFabricatie;
	}

	@Column(name = "tip_combustibil", nullable = false,length=100)
	public String getTipCombustibil() {
		return tipCombustibil;
	}

	public void setTipCombustibil(String tipCombustibil) {
		this.tipCombustibil = tipCombustibil;
	}

	@Column(name = "consum_mediu", nullable = false)
	public Float getConsumMediu() {
		return consumMediu;
	}

	public void setConsumMediu(Float consumMediu) {
		this.consumMediu = consumMediu;
	}

	@Column(name = "cai_putere", nullable = false)
	public Integer getCaiPutere() {
		return caiPutere;
	}

	public void setCaiPutere(Integer caiPutere) {
		this.caiPutere = caiPutere;
	}

	@Column(name = "nr_inmatriculare", nullable = false,length=100)
	public String getNrInmatriculare() {
		return nrInmatriculare;
	}

	public void setNrInmatriculare(String nrInmatriculare) {
		this.nrInmatriculare = nrInmatriculare;
	}

	@Column(name = "nr_locuri", nullable = false)
	public Integer getNrLocuri() {
		return nrLocuri;
	}

	public void setNrLocuri(Integer nrLocuri) {
		this.nrLocuri = nrLocuri;
	}

	@Column(name = "pret_inchiriere", nullable = false)
	public Float getPretInchiriere() {
		return pretInchiriere;
	}

	public void setPretInchiriere(Float pretInchiriere) {
		this.pretInchiriere = pretInchiriere;
	}

	@Column(name = "status_masina", nullable = false,length=100)
	public String getStatusMasina() {
		return statusMasina;
	}

	public void setStatusMasina(String statusMasina) {
		this.statusMasina = statusMasina;
	}

	@Column(name = "timp_minim_inchiriere", nullable = false)
	public Integer getTimpMinimInchiriere() {
		return timpMinimInchiriere;
	}

	public void setTimpMinimInchiriere(Integer timpMinimInchiriere) {
		this.timpMinimInchiriere = timpMinimInchiriere;
	}
	
	
}
