package rentacar.entities;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Proxy;

@Entity
@Table(name = "utilizatori")
@Proxy(lazy = false)
public class Utilizator implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private String email;
	private String parola;
	private String rol;
	
	public Utilizator() {}

	public Utilizator(Integer id, String email, String parola, String rol) {
		super();
		this.id = id;
		this.email = email;
		this.parola = parola;
		this.rol = rol;
	}


	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "email", unique = true, nullable = false,length=100)
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getParola() {
		return parola;
	}

	@Column(name = "parola", nullable = false,length=100)
	public void setParola(String parola) {
		this.parola = parola;
	}

	@Column(name = "rol",  nullable = false,length=100)
	public String getRol() {
		return rol;
	}

	public void setRol(String rol) {
		this.rol = rol;
	}
	
}
