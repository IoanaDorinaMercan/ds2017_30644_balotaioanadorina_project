package rentacar.entities;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Proxy;
import org.springframework.transaction.annotation.Transactional;


@Entity
@Table(name = "locatii")
@Transactional
@Proxy(lazy = false)
public class Locatie  implements java.io.Serializable{

	private static final long serialVersionUID = 1L;
	
	private Integer id;
	private String nume;
	private String judet;
	private Float latitudine;
	private Float longitudine;
	
	public Locatie() {}

	public Locatie(Integer id, String nume, String judet, float latitudine, float longitudine) {
		super();
		this.id = id;
		this.nume = nume;
		this.judet = judet;
		this.latitudine = latitudine;
		this.longitudine = longitudine;
	}
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	@Column(name = "nume",nullable = false,length=100)
	public String getNume() {
		return nume;
	}

	public void setNume(String nume) {
		this.nume = nume;
	}

	@Column(name = "judet",nullable = false,length=100)
	public String getJudet() {
		return judet;
	}

	public void setJudet(String judet) {
		this.judet = judet;
	}

	@Column(name = "latitudine",nullable = false,columnDefinition = "float(5,2)")
	public Float getLatitudine() {
		return latitudine;
	}

	public void setLatitudine(float latitudine) {
		this.latitudine = latitudine;
	}

	@Column(name = "longitudine",nullable = false,columnDefinition = "float(5,2)")
	public Float getLongitudine() {
		return longitudine;
	}

	public void setLongitudine(float longitudine) {
		this.longitudine = longitudine;
	};
	
	
}
