package rentacar.services;

import java.sql.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import rentacar.dto.InchiriereMasinaDTO;
import rentacar.dto.MasinaDTO;
import rentacar.entities.InchiriereMasina;
import rentacar.repositories.InchiriereMasinaRepository;

@Service
@Transactional
public class InchiriereMasinaService {

	@Autowired
	private InchiriereMasinaRepository inchiriereMasinaRepository;
	
	
	public int create(InchiriereMasinaDTO inchiriereMasinaDTO)
	{
		/*if(inchiriereMasinaRepository.findByMasinaIdAndDataStartBetweenAndDataStopBetween(inchiriereMasinaDTO.getMasina().getId(), inchiriereMasinaDTO.getDataStart(), inchiriereMasinaDTO.getDataStop(), inchiriereMasinaDTO.getDataStart(), inchiriereMasinaDTO.getDataStop())==null)
		{
			InchiriereMasina inchiriereMasina=new InchiriereMasina();
			inchiriereMasina.setClient(inchiriereMasinaDTO.getClient());
			inchiriereMasina.setDataStart(inchiriereMasinaDTO.getDataStart());
			inchiriereMasina.setDataStop(inchiriereMasinaDTO.getDataStop());
			inchiriereMasina.setLocatiePredare(inchiriereMasinaDTO.getLocatiePredare());
			inchiriereMasina.setLocatieRidicare(inchiriereMasinaDTO.getLocatieRidicare());
			inchiriereMasina.setMasina(inchiriereMasinaDTO.getMasina());
			
			return inchiriereMasinaRepository.save(inchiriereMasina).getId();
		}
		return -1;*/
		
		//if(inchiriereMasinaRepository.findLastByMasinaIdAndDataStartLessThanAndDataStopLessThan(inchiriereMasinaDTO.getMasina().getId(), inchiriereMasinaDTO.getDataStart(), inchiriereMasinaDTO.getDataStop(),inchiriereMasinaDTO.getDataStop())==null)
		//{
			/*if(inchiriereMasinaRepository.findLastByMasinaIdAndDataStartGreaterThanAndDataStopGreaterThanAndDataStartLessThan(inchiriereMasinaDTO.getMasina().getId(), inchiriereMasinaDTO.getDataStart(), inchiriereMasinaDTO.getDataStop(),inchiriereMasinaDTO.getDataStart())==null)
			{
				if(inchiriereMasinaRepository.findLastByMasinaIdAndDataStartLessThanAndDataStopLessThanAndDataStopGreaterThan(inchiriereMasinaDTO.getMasina().getId(), inchiriereMasinaDTO.getDataStart(), inchiriereMasinaDTO.getDataStop(),inchiriereMasinaDTO.getDataStart())==null)
				{
					InchiriereMasina inchiriereMasina=new InchiriereMasina();
					inchiriereMasina.setClient(inchiriereMasinaDTO.getClient());
					inchiriereMasina.setDataStart(inchiriereMasinaDTO.getDataStart());
					inchiriereMasina.setDataStop(inchiriereMasinaDTO.getDataStop());
					inchiriereMasina.setLocatiePredare(inchiriereMasinaDTO.getLocatiePredare());
					inchiriereMasina.setLocatieRidicare(inchiriereMasinaDTO.getLocatieRidicare());
					inchiriereMasina.setMasina(inchiriereMasinaDTO.getMasina());
					
					return inchiriereMasinaRepository.save(inchiriereMasina).getId();
				}
			}*/
		if(inchiriereMasinaRepository.findByMasinaIdAndDataStartLessThanAndDataStopLessThanAndDataStopGreaterThan(inchiriereMasinaDTO.getMasina().getId(), inchiriereMasinaDTO.getDataStart(), inchiriereMasinaDTO.getDataStop(),inchiriereMasinaDTO.getDataStart())==null||inchiriereMasinaRepository.findByMasinaIdAndDataStartLessThanAndDataStopLessThanAndDataStopGreaterThan(inchiriereMasinaDTO.getMasina().getId(), inchiriereMasinaDTO.getDataStart(), inchiriereMasinaDTO.getDataStop(),inchiriereMasinaDTO.getDataStart()).isEmpty())
		{
			if(inchiriereMasinaRepository.findByMasinaIdAndDataStartLessThanAndDataStopGreaterThan(inchiriereMasinaDTO.getMasina().getId(),inchiriereMasinaDTO.getDataStart(),inchiriereMasinaDTO.getDataStop())==null||inchiriereMasinaRepository.findByMasinaIdAndDataStartLessThanAndDataStopGreaterThan(inchiriereMasinaDTO.getMasina().getId(),inchiriereMasinaDTO.getDataStart(),inchiriereMasinaDTO.getDataStop()).isEmpty())
			{
				if(inchiriereMasinaRepository.findByMasinaIdAndDataStartGreaterThanAndDataStopGreaterThanAndDataStartLessThan(inchiriereMasinaDTO.getMasina().getId(),inchiriereMasinaDTO.getDataStart(),inchiriereMasinaDTO.getDataStop(),inchiriereMasinaDTO.getDataStop())==null||inchiriereMasinaRepository.findByMasinaIdAndDataStartGreaterThanAndDataStopGreaterThanAndDataStartLessThan(inchiriereMasinaDTO.getMasina().getId(),inchiriereMasinaDTO.getDataStart(),inchiriereMasinaDTO.getDataStop(),inchiriereMasinaDTO.getDataStop()).isEmpty())
			{InchiriereMasina inchiriereMasina=new InchiriereMasina();
			inchiriereMasina.setClient(inchiriereMasinaDTO.getClient());
			inchiriereMasina.setDataStart(inchiriereMasinaDTO.getDataStart());
			inchiriereMasina.setDataStop(inchiriereMasinaDTO.getDataStop());
			inchiriereMasina.setLocatiePredare(inchiriereMasinaDTO.getLocatiePredare());
			inchiriereMasina.setLocatieRidicare(inchiriereMasinaDTO.getLocatieRidicare());
			inchiriereMasina.setMasina(inchiriereMasinaDTO.getMasina());
			
			return inchiriereMasinaRepository.save(inchiriereMasina).getId();
			}
		}}
		else
			{
				List<InchiriereMasina> myList=inchiriereMasinaRepository.findByMasinaIdAndDataStartLessThanAndDataStopLessThanAndDataStopGreaterThan(inchiriereMasinaDTO.getMasina().getId(), inchiriereMasinaDTO.getDataStart(), inchiriereMasinaDTO.getDataStop(),inchiriereMasinaDTO.getDataStart());
			System.out.println(myList.size());
				return -1;
			}
		return -1;
		
	}
	
	public InchiriereMasinaDTO findById(int id)
	{
		InchiriereMasina inchiriereMasina=inchiriereMasinaRepository.findById(id);
		InchiriereMasinaDTO inchiriereMasinaDTO=new InchiriereMasinaDTO.Builder()
								.client(inchiriereMasina.getClient())
								.dataStart(inchiriereMasina.getDataStart())
								.dataStop(inchiriereMasina.getDataStop())
								.id(inchiriereMasina.getId())
								.locatiePredare(inchiriereMasina.getLocatiePredare())
								.locatieRidicare(inchiriereMasina.getLocatieRidicare())
								.masina(inchiriereMasina.getMasina())
								.create();
		return inchiriereMasinaDTO;
		
	}
	
	public InchiriereMasinaDTO deleteById(int id)
	{
		InchiriereMasina inchiriereMasina=inchiriereMasinaRepository.deleteById(id).get(0);
		InchiriereMasinaDTO inchiriereMasinaDTO=new InchiriereMasinaDTO.Builder()
								.client(inchiriereMasina.getClient())
								.dataStart(inchiriereMasina.getDataStart())
								.dataStop(inchiriereMasina.getDataStop())
								.id(inchiriereMasina.getId())
								.locatiePredare(inchiriereMasina.getLocatiePredare())
								.locatieRidicare(inchiriereMasina.getLocatieRidicare())
								.masina(inchiriereMasina.getMasina())
								.create();
		return inchiriereMasinaDTO;
	}
}
