package rentacar.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rentacar.dto.UtilizatorDTO;
import rentacar.entities.Utilizator;
import rentacar.repositories.UtilizatorRepository;

@Service
public class UtilizatorService {
	
	@Autowired
	private UtilizatorRepository utilizatorRepository;
	
	public int create(UtilizatorDTO utilizatorDTO)
	{
		//verify if no existing user with this email
		if(utilizatorRepository.findByEmail(utilizatorDTO.getEmail())==null)
		{
			Utilizator utilizator=new Utilizator();
			utilizator.setEmail(utilizatorDTO.getEmail());
			utilizator.setParola(utilizatorDTO.getParola());
			utilizator.setRol(utilizatorDTO.getRol());
			
			return utilizatorRepository.save(utilizator).getId();
			
		}
		else
		{
			//throw an exception
		}
		return -1;
	}

	public int create(String email,String password)
	{
		//verify if no existing user with this email
		if(utilizatorRepository.findByEmail(email)==null)
		{
			Utilizator utilizator=new Utilizator();
			utilizator.setEmail(email);
			utilizator.setParola(password);
			utilizator.setRol("admin");
			
			return utilizatorRepository.save(utilizator).getId();
			
		}
		else
		{
			return utilizatorRepository.findByEmail(email).getId();
		}

	}
	public List<UtilizatorDTO> findAll()
	{
		List<Utilizator> utilizator=utilizatorRepository.findAll();
		List<UtilizatorDTO> utilizatorDTO=new ArrayList<UtilizatorDTO>();
		for(int i=0;i<utilizator.size();i++)
			utilizatorDTO.add(new UtilizatorDTO.Builder()
									.id(utilizator.get(i).getId())
									.email(utilizator.get(i).getEmail())
									.parola(utilizator.get(i).getParola())
									.rol(utilizator.get(i).getRol())
									.create());
		
		return utilizatorDTO;
		
	}
	
	public UtilizatorDTO login(String email,String parola)
	{
		Utilizator utilizator=utilizatorRepository.findByEmailAndParola(email, parola);
		
		UtilizatorDTO utilizatorDTO=new UtilizatorDTO.Builder()
									.id(utilizator.getId())
									.email(utilizator.getEmail())
									.parola(utilizator.getParola())
									.rol(utilizator.getRol())
									.create();
		return utilizatorDTO;
		
	}
	public UtilizatorDTO findById(int id)
	{
		Utilizator utilizator=utilizatorRepository.findById(id);
		
		UtilizatorDTO utilizatorDTO=new UtilizatorDTO.Builder()
									.id(utilizator.getId())
									.email(utilizator.getEmail())
									.parola(utilizator.getParola())
									.rol(utilizator.getRol())
									.create();
		return utilizatorDTO;
		
	}
	
	public UtilizatorDTO findByEmail(String email)
	{
		System.out.println(email);
		Utilizator utilizator=utilizatorRepository.findByEmail(email);
		
		UtilizatorDTO utilizatorDTO=new UtilizatorDTO.Builder()
									.id(utilizator.getId())
									.email(utilizator.getEmail())
									.parola(utilizator.getParola())
									.rol(utilizator.getRol())
									.create();
		return utilizatorDTO;
		
	}
	
	public UtilizatorDTO deleteById(int id)
	{
		Utilizator utilizator=utilizatorRepository.deleteById(id).get(0);
		
		UtilizatorDTO utilizatorDTO=new UtilizatorDTO.Builder()
									.id(utilizator.getId())
									.email(utilizator.getEmail())
									.parola(utilizator.getParola())
									.rol(utilizator.getRol())
									.create();
		return utilizatorDTO;
		
	}
	
	public UtilizatorDTO deleteByEmail(String email)
	{
		Utilizator utilizator=utilizatorRepository.deleteByEmail(email).get(0);
		
		UtilizatorDTO utilizatorDTO=new UtilizatorDTO.Builder()
									.id(utilizator.getId())
									.email(utilizator.getEmail())
									.parola(utilizator.getParola())
									.rol(utilizator.getRol())
									.create();
		return utilizatorDTO;
		
	}
	
	public UtilizatorDTO updateParolaById(int id,String parola)
	{
		Utilizator utilizator=utilizatorRepository.findById(id);
		utilizator.setParola(parola);
		UtilizatorDTO utilizatorDTO=new UtilizatorDTO.Builder()
									.id(utilizator.getId())
									.email(utilizator.getEmail())
									.parola(utilizator.getParola())
									.rol(utilizator.getRol())
									.create();
		return utilizatorDTO;
		
	}
	
	public UtilizatorDTO updateParolaByEmail(String email,String parola)
	{
		Utilizator utilizator=utilizatorRepository.findByEmail(email);
		utilizator.setParola(parola);
		UtilizatorDTO utilizatorDTO=new UtilizatorDTO.Builder()
									.id(utilizator.getId())
									.email(utilizator.getEmail())
									.parola(utilizator.getParola())
									.rol(utilizator.getRol())
									.create();
		return utilizatorDTO;
		
	}
}
