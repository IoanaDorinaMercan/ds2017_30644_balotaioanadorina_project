package rentacar.services;

import java.io.IOException;
import java.io.InputStream;
import java.io.SequenceInputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.parsing.Location;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonPointer;

import rentacar.entities.AbonatNewsletter;
import rentacar.entities.InchiriereMasina;
import rentacar.entities.Locatie;
import rentacar.entities.Masina;
import rentacar.repositories.AbonatNewsletterRepository;
import rentacar.repositories.InchiriereMasinaRepository;
import rentacar.repositories.LocatieRepository;
import rentacar.repositories.MasinaRepository;

import org.apache.commons.io.IOUtils;
import org.apache.jasper.tagplugins.jstl.core.If;
import org.json.JSONObject;

@Service
public class ComplexService {

	@Autowired
	private InchiriereMasinaRepository inchiriereMasinaRepository;

	@Autowired
	private MasinaRepository masinaRepository;
	
	@Autowired
	private AbonatNewsletterRepository abonatNewsletterRepository;


	private final String API_GOOGLE_MAPS = "AIzaSyAD6PCZht5XFU25nyGe4HzLXhcDDYbkcJ0";

	public Double calculateDistanceBetweenTwoLocations(String locatie1, String locatie2) throws IOException {
		String maps_uri = "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=" + locatie1
				+ "&destinations=" + locatie2 + "&key=" + API_GOOGLE_MAPS;
		URL url = new URL(maps_uri);
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		InputStream inputStream = connection.getInputStream();
		JSONObject json1 = new JSONObject(IOUtils.toString(inputStream));
		JSONObject obj1 = (JSONObject) (((JSONObject) (json1.getJSONArray("rows").get(0))).getJSONArray("elements")
				.get(0));
		String distanceMile = ((JSONObject) obj1.get("distance")).getString("text").toString();
		String[] distance = distanceMile.split("mi");

		return Double.parseDouble(distance[0]) * 1.609;
	}

	// distanta dintre o locatie si locatiile tuturor masinilor disponibile spre
	// inchiriere intr-un anumit moment de timp

	//TODO: trateaza exceptiile
	public JSONObject calculateNearestAvailableCar(String locatieCurenta, Timestamp dataInceput, Timestamp dataSfarsit) throws IOException {
		// verificam toate inchirierile in intervalul selectat de timp
		List<InchiriereMasina> inchirieri = inchiriereMasinaRepository
				.findByDataStartBetweenAndDataStopBetween(dataInceput, dataSfarsit, dataInceput, dataSfarsit);

		// luam toate masinile din baza de date
		List<Masina> masinaList = masinaRepository.findAll();

		// verificam care dintre masini nu se afla printre cele inchiriate
		List<Masina> masiniDisponibile = new ArrayList<Masina>();

		for (int j = 0; j < masinaList.size(); j++) {
			boolean p = false;
			for (int i = 0; i < inchirieri.size(); i++) {
				
				if (masinaList.get(j).getId() == inchirieri.get(i).getMasina().getId())
					p = true;
			}
			if (p == false)
				masiniDisponibile.add(masinaList.get(j));
		}

		Map<Masina,Locatie>ultimaLocatieMasina=new HashMap<>();
		
		//vedem care a fost ultima locatie de predare a masinilor disponibile
		//in cazul in care nu se gaseste, atunci default e cluj-napoca
		for(int i=0;i<masiniDisponibile.size();i++)
		{
			Locatie location=inchiriereMasinaRepository.findLastByMasinaIdAndDataStopLessThan(masiniDisponibile.get(i).getId(), dataInceput).get(0).getLocatiePredare();
		
			if(location==null)
			{
				location=new Locatie();
				location.setNume("Cluj-Napoca");
				location.setJudet("Cluj");
				location.setLatitudine(23);
				location.setLongitudine(24);
				
			}
			System.out.println(location.getNume());
			
			//adaugam masinile disponibile impreuna cu utlima locatie
			ultimaLocatieMasina.put(masiniDisponibile.get(i), location);
		}
		
		//calculam distanta dintre locatia curenta si locatiile curente ale masinilor disponibile
		double minDistance=calculateDistanceBetweenTwoLocations(locatieCurenta, ultimaLocatieMasina.get(masiniDisponibile.get(0)).getNume());
		Masina masinaFinala=new Masina();
		Locatie locatieFinala=new Locatie();
		for(int i=0;i<masiniDisponibile.size();i++)
		{
			double aux=calculateDistanceBetweenTwoLocations(locatieCurenta, ultimaLocatieMasina.get(masiniDisponibile.get(i)).getNume());
			if(aux<=minDistance)
			{
				masinaFinala=masiniDisponibile.get(i);
				locatieFinala=ultimaLocatieMasina.get(masiniDisponibile.get(i));
				System.out.println(locatieFinala.getNume());
				minDistance=aux;
			}
			
		}
		
		JSONObject json1 = new JSONObject(masinaFinala);
		json1.put("distanta", minDistance);
		JSONObject json2 = new JSONObject(locatieFinala);
		json1.put("locatie",json2);
		System.out.println(json1.toString());
		
		return json1;
	}
	
	//TODO
	public void sendNewsletter(String username,String password, String subject,String content)
	{
		//vezi care sunt abonatii la newslette
		List<AbonatNewsletter>abonati=abonatNewsletterRepository.findAll();
		
		//ia doar adresele de email
		List<String>emails=new ArrayList<>();
		
		for(int i=0;i<abonati.size();i++)
			emails.add(abonati.get(i).getEmail());
		
		//trimite mail
		MailService mailService=new MailService(username, password);
		mailService.sendMail(emails, subject, content);

	}
	
	
}
