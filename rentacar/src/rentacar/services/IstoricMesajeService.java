package rentacar.services;


import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import rentacar.dto.IstoricMesajeDTO;
import rentacar.entities.IstoricMesaje;
import rentacar.repositories.IstoricMesajeRepository;

@Service
@Transactional
public class IstoricMesajeService {

	@Autowired
	private IstoricMesajeRepository istoricMesajeRepository;
	
	public int create(IstoricMesajeDTO istoricMesajeDTO)
	{
	
		System.out.println(istoricMesajeDTO.getDataTrimitere());
		IstoricMesaje istoricMesaje=new IstoricMesaje();
		istoricMesaje.setAdmin(istoricMesajeDTO.getAdmin());
		istoricMesaje.setDataTrimitere(istoricMesajeDTO.getDataTrimitere());
		istoricMesaje.setMesaj(istoricMesajeDTO.getMesaj());
		istoricMesaje.setNrPersoane(istoricMesajeDTO.getNrPersoane());
		
		return istoricMesajeRepository.save(istoricMesaje).getId();
	}
	
	public List<IstoricMesajeDTO>findAll() {
		List<IstoricMesaje> istoricMesaje=istoricMesajeRepository.findAll();
		List<IstoricMesajeDTO> istoricMesajeDTO=new ArrayList<IstoricMesajeDTO>();
		for(int i=0;i<istoricMesaje.size();i++)
			istoricMesajeDTO.add(new IstoricMesajeDTO.Builder()
					.dataTrimitere(istoricMesaje.get(i).getDataTrimitere())
					.id(istoricMesaje.get(i).getId())
					.mesaj(istoricMesaje.get(i).getMesaj())
					.nrPersoane(istoricMesaje.get(i).getNrPersoane())
					.utilizator(istoricMesaje.get(i).getAdmin())
					.create());
				
		return istoricMesajeDTO;
	}
	public IstoricMesajeDTO findById(int id) {
		IstoricMesaje istoricMesaje=istoricMesajeRepository.findById(id);
		IstoricMesajeDTO istoricMesajeDTO=new IstoricMesajeDTO.Builder()
				.dataTrimitere(istoricMesaje.getDataTrimitere())
				.id(istoricMesaje.getId())
				.mesaj(istoricMesaje.getMesaj())
				.nrPersoane(istoricMesaje.getNrPersoane())
				.utilizator(istoricMesaje.getAdmin())
				.create();
		return istoricMesajeDTO;
	}
	
	public IstoricMesajeDTO deleteById(int id) {
		IstoricMesaje istoricMesaje=istoricMesajeRepository.deleteById(id).get(0);
		IstoricMesajeDTO istoricMesajeDTO=new IstoricMesajeDTO.Builder()
				.dataTrimitere(istoricMesaje.getDataTrimitere())
				.id(istoricMesaje.getId())
				.mesaj(istoricMesaje.getMesaj())
				.nrPersoane(istoricMesaje.getNrPersoane())
				.utilizator(istoricMesaje.getAdmin())
				.create();
		return istoricMesajeDTO;
	}
}
