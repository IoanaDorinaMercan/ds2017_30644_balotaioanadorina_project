package rentacar.services;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import java.util.List;
import java.util.Properties;

public class MailService {
    final String username;
    final String password;
    final Properties props;
    
	public MailService(String username, String password) {
		
		 this.username = username;
	     this.password = password;
	     
		props = new Properties();
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.socketFactory.port", "465");
		props.put("mail.smtp.socketFactory.class",
				"javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", "465");

	}
	
    public void sendMail(List<String>to, String subject, String content) {
		Session session = Session.getInstance(props,
			new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(username,password);
				}
			});

		try {
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(username));
			for(int i=0;i<to.size();i++)
			message.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse(to.get(i)));
			message.setSubject(subject);
			message.setText(content);

			Transport.send(message);

		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);

		}
	}



}
