package rentacar.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import rentacar.dto.MasinaDTO;
import rentacar.entities.Masina;
import rentacar.repositories.MasinaRepository;

@Service
@Transactional
public class MasinaService {
	
	@Autowired
	private MasinaRepository masinaRepository;

	public int create(MasinaDTO masinaDTO)
	{
		Masina masina=new Masina();
		masina.setAnFabricatie(masinaDTO.getAnFabricatie());
		masina.setCaiPutere(masinaDTO.getCaiPutere());
		masina.setConsumMediu(masinaDTO.getConsumMediu());
		masina.setMarca(masinaDTO.getMarca());
		masina.setModel(masinaDTO.getModel());
		masina.setNrInmatriculare(masinaDTO.getNrInmatriculare());
		masina.setNrLocuri(masinaDTO.getNrLocuri());
		masina.setPretInchiriere(masinaDTO.getPretInchiriere());
		masina.setStatusMasina(masinaDTO.getStatusMasina());
		masina.setTimpMinimInchiriere(masinaDTO.getTimpMinimInchiriere());
		masina.setTipCombustibil(masinaDTO.getTipCombustibil());
		
		return masinaRepository.save(masina).getId();
	}
	
	public MasinaDTO findMasinaById(int masinaId)
	{
		Masina masina=masinaRepository.findById(masinaId);
		//TODO: VERIFY IF RESULT IS NULL
		MasinaDTO masinaDTO=new MasinaDTO.Builder()
							.id(masina.getId())
							.marca(masina.getMarca())
							.model(masina.getModel())
							.anFabricatie(masina.getAnFabricatie())
							.tipCombustibil(masina.getTipCombustibil())
							.consumMediu(masina.getConsumMediu())
							.caiPutere(masina.getCaiPutere())
							.nrInmatriculare(masina.getNrInmatriculare())
							.nrLocuri(masina.getNrLocuri())
							.pretInchiriere(masina.getPretInchiriere())
							.statusMasina(masina.getStatusMasina())
							.timpMinimInchiriere(masina.getTimpMinimInchiriere())
							.create();
		return masinaDTO;
	}
	
	
	public List<MasinaDTO> findAll()
	{
		List<Masina> masina=masinaRepository.findAll();
		//TODO: VERIFY IF RESULT IS NULL
		List<MasinaDTO> masinaDTO=new ArrayList<MasinaDTO>();
		for(int i=0;i<masina.size();i++)
		masinaDTO.add(new MasinaDTO.Builder()
							.id(masina.get(i).getId())
							.marca(masina.get(i).getMarca())
							.model(masina.get(i).getModel())
							.anFabricatie(masina.get(i).getAnFabricatie())
							.tipCombustibil(masina.get(i).getTipCombustibil())
							.consumMediu(masina.get(i).getConsumMediu())
							.caiPutere(masina.get(i).getCaiPutere())
							.nrInmatriculare(masina.get(i).getNrInmatriculare())
							.nrLocuri(masina.get(i).getNrLocuri())
							.pretInchiriere(masina.get(i).getPretInchiriere())
							.statusMasina(masina.get(i).getStatusMasina())
							.timpMinimInchiriere(masina.get(i).getTimpMinimInchiriere())
							.create());
		return masinaDTO;
	}
	
	public MasinaDTO deleteMasinaById(int masinaId)
	{
		Masina masina=masinaRepository.deleteById(masinaId).get(0);
		//TODO: VERIFY IF RESULT IS NULL
		MasinaDTO masinaDTO=new MasinaDTO.Builder()
							.id(masina.getId())
							.marca(masina.getMarca())
							.model(masina.getModel())
							.anFabricatie(masina.getAnFabricatie())
							.tipCombustibil(masina.getTipCombustibil())
							.consumMediu(masina.getConsumMediu())
							.caiPutere(masina.getCaiPutere())
							.nrInmatriculare(masina.getNrInmatriculare())
							.nrLocuri(masina.getNrLocuri())
							.pretInchiriere(masina.getPretInchiriere())
							.statusMasina(masina.getStatusMasina())
							.timpMinimInchiriere(masina.getTimpMinimInchiriere())
							.create();
		return masinaDTO;
	}
	
	public MasinaDTO updatePretInchiriereById(int masinaId,float pretInchiriere)
	{
		Masina masina=masinaRepository.findById(masinaId);
		//TODO: VERIFY IF RESULT IS NULL
		//TODO: verify if parameters are positive
		masina.setPretInchiriere(pretInchiriere);
		
		MasinaDTO masinaDTO=new MasinaDTO.Builder()
							.id(masina.getId())
							.marca(masina.getMarca())
							.model(masina.getModel())
							.anFabricatie(masina.getAnFabricatie())
							.tipCombustibil(masina.getTipCombustibil())
							.consumMediu(masina.getConsumMediu())
							.caiPutere(masina.getCaiPutere())
							.nrInmatriculare(masina.getNrInmatriculare())
							.nrLocuri(masina.getNrLocuri())
							.pretInchiriere(masina.getPretInchiriere())
							.statusMasina(masina.getStatusMasina())
							.timpMinimInchiriere(masina.getTimpMinimInchiriere())
							.create();
		return masinaDTO;
	}
	
	public MasinaDTO updateTimpMinimInchiriereById(int masinaId,int timpMinimInchiriere)
	{
		Masina masina=masinaRepository.findById(masinaId);
		//TODO: VERIFY IF RESULT IS NULL
		//TODO: verify if parameters are positive
		masina.setTimpMinimInchiriere(timpMinimInchiriere);
		MasinaDTO masinaDTO=new MasinaDTO.Builder()
							.id(masina.getId())
							.marca(masina.getMarca())
							.model(masina.getModel())
							.anFabricatie(masina.getAnFabricatie())
							.tipCombustibil(masina.getTipCombustibil())
							.consumMediu(masina.getConsumMediu())
							.caiPutere(masina.getCaiPutere())
							.nrInmatriculare(masina.getNrInmatriculare())
							.nrLocuri(masina.getNrLocuri())
							.pretInchiriere(masina.getPretInchiriere())
							.statusMasina(masina.getStatusMasina())
							.timpMinimInchiriere(masina.getTimpMinimInchiriere())
							.create();
		return masinaDTO;
	}
}
