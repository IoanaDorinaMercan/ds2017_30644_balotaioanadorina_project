package rentacar.services;

import java.sql.Date;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mysql.fabric.xmlrpc.Client;

import rentacar.dto.CodActivareDTO;
import rentacar.entities.CodActivare;
import rentacar.repositories.ClientRepository;
import rentacar.repositories.CodActivareRepository;

@Service
@Transactional
public class CodActivareService {
	
	@Autowired
	private CodActivareRepository codActivareRepository;
	
	@Autowired
	private ClientRepository clientRepository;
	
	public int create(int clientId)
	{
		Random rand = new Random();

		int  n = rand.nextInt(89999) + 10000;
		while(codActivareRepository.findByCod(String.valueOf(n))!=null)
		{
			n=rand.nextInt(89999) + 10000;
		}
		
		rentacar.entities.Client client=clientRepository.findById(clientId);
		
		if(codActivareRepository.findByCod(String.valueOf(n))==null)
		{
			CodActivare codActivare=new CodActivare();
			codActivare.setActivat(false);
			codActivare.setClient(client);
			codActivare.setCod(String.valueOf(n));
			codActivare.setDataActivarii(new java.sql.Date(System.currentTimeMillis()));
			return codActivareRepository.save(codActivare).getId();
		}
		return -1;
	}

	
	public  CodActivareDTO findByCod(String cod)
	{
		CodActivare codActivare=codActivareRepository.findByCod(cod);
		CodActivareDTO codActivareDTO=new CodActivareDTO.Builder()
										.activat(codActivare.getActivat())
										.client(codActivare.getClient())
										.cod(codActivare.getCod())
										.dataActivarii(codActivare.getDataActivarii())
										.id(codActivare.getId())
										.create();
		return codActivareDTO;
										
										
	}
	
	public CodActivareDTO deleteByCod(String cod)
	{
		CodActivare codActivare=codActivareRepository.deleteByCod(cod).get(0);
		CodActivareDTO codActivareDTO=new CodActivareDTO.Builder()
										.activat(codActivare.getActivat())
										.client(codActivare.getClient())
										.cod(codActivare.getCod())
										.dataActivarii(codActivare.getDataActivarii())
										.id(codActivare.getId())
										.create();
		return codActivareDTO;
	}
	
	public CodActivareDTO updateStatusAndDataActivariiByCod(String cod,boolean status,Date dataActivarii)
	{
		CodActivare codActivare=codActivareRepository.findByCod(cod);
		codActivare.setActivat(status);
		codActivare.setDataActivarii(dataActivarii);
		
		CodActivareDTO codActivareDTO=new CodActivareDTO.Builder()
										.id(codActivare.getId())
										.activat(codActivare.getActivat())
										.client(codActivare.getClient())
										.cod(codActivare.getCod())
										.dataActivarii(codActivare.getDataActivarii())
										.create();
		return codActivareDTO;
	}
}
