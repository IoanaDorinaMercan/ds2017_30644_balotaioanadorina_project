package rentacar.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import rentacar.dto.RatingDTO;
import rentacar.entities.Rating;
import rentacar.repositories.RatingRepository;

@Service
@Transactional
public class RatingService {

	@Autowired
	private RatingRepository ratingRepository;
	
	public int create(RatingDTO ratingDTO)
	{
		if(ratingRepository.findByClientEmailAndMasinaId(ratingDTO.getClient().getEmail(), ratingDTO.getMasina().getId())==null)
		{
			Rating rating=new Rating();
			rating.setComentariu(ratingDTO.getComentariu());
			rating.setNota(ratingDTO.getNota());
			rating.setMasina(ratingDTO.getMasina());
			rating.setClient(ratingDTO.getClient());
			
			
			return ratingRepository.save(rating).getId();
		}
		return -1;
	}
	
	public RatingDTO findByClientEmailAndMasinaId(String clientEmail,int masinaId)
	{
		Rating rating=ratingRepository.findByClientEmailAndMasinaId(clientEmail, masinaId);
		RatingDTO ratingDTO=new RatingDTO.Builder()
							.client(rating.getClient())
							.comentariu(rating.getComentariu())
							.id(rating.getId())
							.masina(rating.getMasina())
							.nota(rating.getNota())
							.create();
		return ratingDTO;
	}
	
	public RatingDTO deleteById(int id)
	{
		Rating rating=ratingRepository.deleteById(id).get(0);
		RatingDTO ratingDTO=new RatingDTO.Builder()
							.client(rating.getClient())
							.comentariu(rating.getComentariu())
							.id(rating.getId())
							.masina(rating.getMasina())
							.nota(rating.getNota())
							.create();
		return ratingDTO;
	}
	
	public RatingDTO updateNotaById(int id,float nota)
	{
		
		Rating rating=ratingRepository.findById(id);
		rating.setNota(nota);
		RatingDTO ratingDTO=new RatingDTO.Builder()
							.client(rating.getClient())
							.comentariu(rating.getComentariu())
							.id(rating.getId())
							.masina(rating.getMasina())
							.nota(rating.getNota())
							.create();
		return ratingDTO;
	}
	
	public RatingDTO updateComentariuById(int id,String comentariu)
	{
		
		Rating rating=ratingRepository.findById(id);
		rating.setComentariu(comentariu);
		RatingDTO ratingDTO=new RatingDTO.Builder()
							.client(rating.getClient())
							.comentariu(rating.getComentariu())
							.id(rating.getId())
							.masina(rating.getMasina())
							.nota(rating.getNota())
							.create();
		return ratingDTO;
	}
	
	
}
