package rentacar.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rentacar.dto.LocatieDTO;
import rentacar.entities.Locatie;
import rentacar.repositories.LocatieRepository;

@Service
public class LocatieService {

	@Autowired
	private LocatieRepository locatieRepository;

	public int create(LocatieDTO locatieDTO) {
		float delta=(float) 0.00001;
		/*if (locatieRepository.findByLatitudineBetweenAndLongitudineBetween(locatieDTO.getLatitudine()-delta,locatieDTO.getLatitudine()+delta,
				locatieDTO.getLongitudine()-delta,locatieDTO.getLongitudine()+delta) == null)*/
		if(locatieRepository.findByNume(locatieDTO.getNume())==null){
			Locatie locatie = new Locatie();
			locatie.setNume(locatieDTO.getNume());
			locatie.setLatitudine(locatieDTO.getLatitudine());
			locatie.setLongitudine(locatieDTO.getLongitudine());
			locatie.setJudet(locatieDTO.getJudet());
			return locatieRepository.save(locatie).getId();

		}

		else return locatieRepository.findByNume(locatieDTO.getNume()).getId();
	}

	public LocatieDTO findLocatieById(int locatieId) {
		Locatie locatie = locatieRepository.findById(locatieId);
		LocatieDTO locatieDTO = new LocatieDTO.Builder().id(locatie.getId()).nume(locatie.getNume())
				.latitudine(locatie.getLatitudine()).longitudine(locatie.getLongitudine()).judet(locatie.getJudet())
				.create();
		return locatieDTO;

	}

	public LocatieDTO findLocatieByNume(String nume) {
		Locatie locatie = locatieRepository.findByNume(nume);
		LocatieDTO locatieDTO = new LocatieDTO.Builder().id(locatie.getId()).nume(locatie.getNume())
				.latitudine(locatie.getLatitudine()).longitudine(locatie.getLongitudine()).judet(locatie.getJudet())
				.create();
		return locatieDTO;

	}

	public LocatieDTO findLocatieByLatitudineAndLongitudine(float latitudine, float longitudine) {
		float delta=(float) 0.00001;
		Locatie locatie = locatieRepository.findByLatitudineBetweenAndLongitudineBetween(latitudine-delta,latitudine+delta, longitudine-delta,longitudine+delta);
		LocatieDTO locatieDTO = new LocatieDTO.Builder()
				.id(locatie.getId())
				.nume(locatie.getNume())
				.latitudine(locatie.getLatitudine())
				.longitudine(locatie.getLongitudine())
				.judet(locatie.getJudet())
				.create();
		return locatieDTO;

	}

	public LocatieDTO deleteByID(int locatieId) {
		Locatie locatie = locatieRepository.deleteById(locatieId).get(0);
		LocatieDTO locatieDTO = new LocatieDTO.Builder().id(locatie.getId()).nume(locatie.getNume())
				.latitudine(locatie.getLatitudine()).longitudine(locatie.getLongitudine()).judet(locatie.getJudet())
				.create();
		return locatieDTO;

	}
	
	public LocatieDTO deleteByLatitudineAndLongitudine(float latitudine, float longitudine) {
		Locatie locatie = locatieRepository.deleteByLatitudineAndLongitudine(latitudine, longitudine);
		LocatieDTO locatieDTO = new LocatieDTO.Builder().id(locatie.getId()).nume(locatie.getNume())
				.latitudine(locatie.getLatitudine()).longitudine(locatie.getLongitudine()).judet(locatie.getJudet())
				.create();
		return locatieDTO;
		
	}
	
	public LocatieDTO updateLocatieNumeByLatitudineAndLongitudine(float latitudine,float longitudine,String nume)
	{
		float delta=(float) 0.00001;
		Locatie locatie = locatieRepository.findByLatitudineBetweenAndLongitudineBetween(latitudine-delta,latitudine+delta, longitudine-delta,longitudine+delta);
		locatie.setNume(nume);
		LocatieDTO locatieDTO = new LocatieDTO.Builder().id(locatie.getId()).nume(locatie.getNume())
				.latitudine(locatie.getLatitudine()).longitudine(locatie.getLongitudine()).judet(locatie.getJudet())
				.create();
		return locatieDTO;
	}
	
	public LocatieDTO updateLocatieNumeLatitudineLongitudineById(int id,String nume,float latitudine,float longitudine)
	{
		Locatie locatie = locatieRepository.findById(id);
		locatie.setNume(nume);
		locatie.setLatitudine(latitudine);
		locatie.setLongitudine(longitudine);
		LocatieDTO locatieDTO = new LocatieDTO.Builder().id(locatie.getId()).nume(locatie.getNume())
				.latitudine(locatie.getLatitudine()).longitudine(locatie.getLongitudine()).judet(locatie.getJudet())
				.create();
		return locatieDTO;
	}
}
