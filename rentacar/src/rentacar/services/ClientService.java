package rentacar.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import rentacar.dto.ClientDTO;
import rentacar.entities.Client;
import rentacar.repositories.ClientRepository;

@Service
@Transactional
public class ClientService {

	@Autowired
	private ClientRepository clientRepository;
	
	public int create(ClientDTO clientDTO)
	{
		if(clientRepository.findByEmail(clientDTO.getEmail())==null)
		{
			if(clientRepository.findByTelefon(clientDTO.getTelefon())==null)
			{
				if(clientRepository.findByCnp(clientDTO.getCnp())==null)
				{
					Client client=new Client();
					client.setAdresa(clientDTO.getAdresa());
					client.setCategoriePermis(clientDTO.getCategoriePermis());
					client.setCnp(clientDTO.getCnp());
					client.setEmail(clientDTO.getEmail());
					client.setNume(clientDTO.getNume());
					client.setObservatii(clientDTO.getObservatii());
					client.setPrenme(clientDTO.getPrenume());
					client.setTelefon(clientDTO.getTelefon());
					client.setVarsta(clientDTO.getVarsta());
					client.setVechimePermis(clientDTO.getVechimePermis());
					
					return clientRepository.save(client).getId();
				}
			}
		}
		return -1;
	}
	
	public ClientDTO findByEmail(String email)
	{
		Client client=clientRepository.findByEmail(email);
		ClientDTO clientDTO=new ClientDTO.Builder()
				.id(client.getId())
				.categoriePermis(client.getCategoriePermis())
				.adresa(client.getAdresa())
				.cnp(client.getCnp())
				.email(client.getCnp())
				.nume(client.getNume())
				.observatii(client.getObservatii())
				.prenume(client.getPrenme())
				.telefon(client.getTelefon())
				.varsta(client.getVarsta())
				.vechimePermis(client.getVechimePermis())
				.create();
		return clientDTO;
	}
	
	public ClientDTO findByTelefon(String telefon)
	{
		Client client=clientRepository.findByTelefon(telefon);
		ClientDTO clientDTO=new ClientDTO.Builder()
				.id(client.getId())
				.categoriePermis(client.getCategoriePermis())
				.adresa(client.getAdresa())
				.cnp(client.getCnp())
				.email(client.getCnp())
				.nume(client.getNume())
				.observatii(client.getObservatii())
				.prenume(client.getPrenme())
				.telefon(client.getTelefon())
				.varsta(client.getVarsta())
				.vechimePermis(client.getVechimePermis())
				.create();
		return clientDTO;
	}
	
	public ClientDTO findByCnp(String cnp)
	{
		Client client=clientRepository.findByCnp(cnp);
		ClientDTO clientDTO=new ClientDTO.Builder()
				.id(client.getId())
				.categoriePermis(client.getCategoriePermis())
				.adresa(client.getAdresa())
				.cnp(client.getCnp())
				.email(client.getCnp())
				.nume(client.getNume())
				.observatii(client.getObservatii())
				.prenume(client.getPrenme())
				.telefon(client.getTelefon())
				.varsta(client.getVarsta())
				.vechimePermis(client.getVechimePermis())
				.create();
		return clientDTO;
	}
	
	public ClientDTO deleteByCnp(String cnp)
	{
		Client client=clientRepository.deleteByCnp(cnp).get(0);
		ClientDTO clientDTO=new ClientDTO.Builder()
				.id(client.getId())
				.categoriePermis(client.getCategoriePermis())
				.adresa(client.getAdresa())
				.cnp(client.getCnp())
				.email(client.getCnp())
				.nume(client.getNume())
				.observatii(client.getObservatii())
				.prenume(client.getPrenme())
				.telefon(client.getTelefon())
				.varsta(client.getVarsta())
				.vechimePermis(client.getVechimePermis())
				.create();
		return clientDTO;
	}
	
	public ClientDTO deleteByEmail(String email)
	{
		Client client=clientRepository.deleteByEmail(email).get(0);
		ClientDTO clientDTO=new ClientDTO.Builder()
				.id(client.getId())
				.categoriePermis(client.getCategoriePermis())
				.adresa(client.getAdresa())
				.cnp(client.getCnp())
				.email(client.getCnp())
				.nume(client.getNume())
				.observatii(client.getObservatii())
				.prenume(client.getPrenme())
				.telefon(client.getTelefon())
				.varsta(client.getVarsta())
				.vechimePermis(client.getVechimePermis())
				.create();
		return clientDTO;
	}
	
	public ClientDTO updateAdresaByEmail(String email,String adresa)
	{
		Client client=clientRepository.findByEmail(email);
		client.setAdresa(adresa);
		ClientDTO clientDTO=new ClientDTO.Builder()
				.id(client.getId())
				.categoriePermis(client.getCategoriePermis())
				.adresa(client.getAdresa())
				.cnp(client.getCnp())
				.email(client.getCnp())
				.nume(client.getNume())
				.observatii(client.getObservatii())
				.prenume(client.getPrenme())
				.telefon(client.getTelefon())
				.varsta(client.getVarsta())
				.vechimePermis(client.getVechimePermis())
				.create();
		return clientDTO;
	}
}
