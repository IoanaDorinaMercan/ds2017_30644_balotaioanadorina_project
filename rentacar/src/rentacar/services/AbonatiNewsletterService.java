package rentacar.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rentacar.dto.AbonatiNewsletterDTO;
import rentacar.entities.AbonatNewsletter;
import rentacar.repositories.AbonatNewsletterRepository;

@Service
public class AbonatiNewsletterService {

	@Autowired
	private AbonatNewsletterRepository abonatNewsletterRepository;
	
	public int create(AbonatiNewsletterDTO abonatiNewsletterDTO) {
		if(abonatNewsletterRepository.findByEmail(abonatiNewsletterDTO.getEmail())==null)
		{
			AbonatNewsletter abonatNewsletter=new AbonatNewsletter();
			abonatNewsletter.setEmail(abonatiNewsletterDTO.getEmail());
			abonatNewsletter.setNume(abonatiNewsletterDTO.getNume());
			
			return abonatNewsletterRepository.save(abonatNewsletter).getId();
		}
		return -1;
	}
	
	public AbonatiNewsletterDTO deleteByEmail(String email)
	{
		AbonatNewsletter abonatNewsletter=abonatNewsletterRepository.deleteByEmail(email).get(0);
		
		AbonatiNewsletterDTO abonatiNewsletterDTO=new AbonatiNewsletterDTO.Builder()
													.id(abonatNewsletter.getId())
													.nume(abonatNewsletter.getNume())
													.email(abonatNewsletter.getEmail())
													.create();
		return abonatiNewsletterDTO;
		
	}
}
