package rentacar.controller;

import java.sql.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import rentacar.dto.CodActivareDTO;
import rentacar.services.CodActivareService;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/cod")
public class CodActivareController {
	
	@Autowired
	private CodActivareService codActivareService;
	
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public int addCod(@RequestParam("clientId") int id) {
		return codActivareService.create(id);
	}

	@RequestMapping(value = "/find", method = RequestMethod.GET)
	public CodActivareDTO findByCod(@RequestParam("cod") String cod) {
		return codActivareService.findByCod(cod);
	}
	
	@RequestMapping(value = "/delete/{cod}", method = RequestMethod.DELETE)
	public CodActivareDTO deleteByCod(@PathVariable("cod") String cod) {
		return codActivareService.deleteByCod(cod);
	}
	
	@RequestMapping(value = "/update/{cod}", method = RequestMethod.PUT)
	public CodActivareDTO updateStatusAndDataActivariiByCod(@PathVariable("cod") String cod,@RequestParam(value="status")boolean status,@RequestParam(value="dataActivarii")Date dataActivarii)
	{

		System.out.println(cod);
		return codActivareService.updateStatusAndDataActivariiByCod(cod, status, dataActivarii);
	}
	
}
