package rentacar.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import rentacar.dto.LocatieDTO;
import rentacar.services.LocatieService;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/locatie")
public class LocatieController {

	@Autowired
	private LocatieService locatieService;
	
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public int addLocatie(@RequestBody LocatieDTO locatieDTO) {
		return locatieService.create(locatieDTO);
	}
	
	@RequestMapping(value = "/find/id/{id}", method = RequestMethod.GET)
	public LocatieDTO findLocatieById(@PathVariable("id") int id) {
		return locatieService.findLocatieById(id);
	}
	
	@RequestMapping(value = "/find/nume/{nume}", method = RequestMethod.GET)
	public LocatieDTO findLocatieByNume(@PathVariable("nume") String nume) {
		return locatieService.findLocatieByNume(nume);
	}
	
	@RequestMapping(value = "/find", method = RequestMethod.GET)
	public LocatieDTO findLocatieByLatitudineAndLongitudine(@RequestParam(value="latitudine")float latitudine,@RequestParam(value="longitudine")float longitudine) {
		return locatieService.findLocatieByLatitudineAndLongitudine(latitudine, longitudine);
	}
	
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	public LocatieDTO deleteLocatieById(@PathVariable("id") int id) {
		return locatieService.deleteByID(id);
	}
	
	@RequestMapping(value = "/delete", method = RequestMethod.DELETE)
	public LocatieDTO deleteLocatieByLatitudineAndLongitudine(@RequestParam(value="latitudine")float latitudine,@RequestParam(value="longitudine")float longitudine) {
		return locatieService.deleteByLatitudineAndLongitudine(latitudine, longitudine);
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	public LocatieDTO updateLocatieNumeByLatitudineAndLongitudine(@RequestParam(value="latitudine")float latitudine,@RequestParam(value="longitudine")float longitudine,@RequestParam(value="nume")String nume) {
		return locatieService.updateLocatieNumeByLatitudineAndLongitudine(latitudine, longitudine, nume);
	}
	
	@RequestMapping(value = "/update/{id}", method = RequestMethod.PUT)
	public LocatieDTO 	updateLocatieNumeLatitudineLongitudineById(@PathVariable("id") int id,@RequestParam(value="latitudine")float latitudine,@RequestParam(value="longitudine")float longitudine,@RequestParam(value="nume")String nume) {
		return locatieService.updateLocatieNumeLatitudineLongitudineById(id, nume, latitudine, longitudine);
	}
}
