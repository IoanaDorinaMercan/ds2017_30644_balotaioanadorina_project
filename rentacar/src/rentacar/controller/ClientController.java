package rentacar.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import rentacar.dto.ClientDTO;
import rentacar.services.ClientService;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/client")
public class ClientController {

	@Autowired
	private ClientService clientService;
	
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public int addClient(@RequestBody ClientDTO clientDTO) {
		return clientService.create(clientDTO);
	}
	
	/*@RequestMapping(value = "/find/email/{email}", method = RequestMethod.GET)
	public ClientDTO findClientByEmail(@PathVariable("email") String email) {
		return clientService.findByEmail(email);
	}*/
	
	@RequestMapping(value = "/findByEmail", method = RequestMethod.GET)
	public ClientDTO findClientByEmail(@RequestParam("email") String email) {
		return clientService.findByEmail(email);
	}
	
	@RequestMapping(value = "/findByCnp", method = RequestMethod.GET)
	public ClientDTO findClientByCnp(@RequestParam("cnp") String cnp) {
		return clientService.findByCnp(cnp);
	}
	
	@RequestMapping(value = "/findByTelefon", method = RequestMethod.GET)
	public ClientDTO findClientByTelefon(@RequestParam("telefon") String telefon) {
		return clientService.findByTelefon(telefon);
	}
	
	@RequestMapping(value = "/deleteByEmail", method = RequestMethod.DELETE)
	public ClientDTO deleteClientByEmail(@RequestParam("email") String email) {
		return clientService.deleteByEmail(email);
	}
	
	@RequestMapping(value = "/deleteByCnp", method = RequestMethod.DELETE)
	public ClientDTO deleteClientByCnp(@RequestParam("cnp") String cnp) {
		return clientService.deleteByCnp(cnp);
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	public ClientDTO updateAdresaByEmail(@RequestParam("email") String email,@RequestParam(value="adresa")String adresa) {
		return clientService.updateAdresaByEmail(email, adresa);
	}
}
