package rentacar.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import rentacar.dto.MasinaDTO;
import rentacar.services.MasinaService;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/masina")
public class MasinaController {
	
	@Autowired
	private MasinaService masinaService;

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public int addMasina(@RequestBody MasinaDTO masinaDTO) {
		return masinaService.create(masinaDTO);
	}
	@RequestMapping(value = "/find/all", method = RequestMethod.GET)
	public List<MasinaDTO> getAllMasina() {
		return masinaService.findAll();
	}
	@RequestMapping(value = "/find/{id}", method = RequestMethod.GET)
	public MasinaDTO getMasinaById(@PathVariable("id") int id) {
		return masinaService.findMasinaById(id);
	}
	
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	public MasinaDTO deleteMasinaById(@PathVariable("id") int id) {
		return masinaService.deleteMasinaById(id);
	}
	
	@RequestMapping(value = "/update/pret/{id}", method = RequestMethod.PUT)
	public MasinaDTO updatePretInchiriereById(@PathVariable("id") int id,@RequestParam(value="pret") float pret) {
		return masinaService.updatePretInchiriereById(id,pret);
	}
	
	@RequestMapping(value = "/update/timp/{id}", method = RequestMethod.PUT)
	public MasinaDTO updateTimpMinimInchiriereById(@PathVariable("id") int id,@RequestParam(value="timp") int timp) {
		return masinaService.updateTimpMinimInchiriereById(id,timp);
	}
}
