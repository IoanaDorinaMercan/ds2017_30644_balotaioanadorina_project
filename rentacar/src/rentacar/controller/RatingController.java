package rentacar.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import rentacar.dto.RatingDTO;
import rentacar.services.RatingService;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/rating")
public class RatingController {

	@Autowired
	private RatingService ratingService;
	
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public int addRating(@RequestBody RatingDTO ratingDTO) {
		return ratingService.create(ratingDTO);
	}
	
	@RequestMapping(value = "/find", method = RequestMethod.GET)
	public RatingDTO findByClientEmailAndMasinaId(@RequestParam(value="email")String email,@RequestParam(value="masinaId")int masinaId) {
		return ratingService.findByClientEmailAndMasinaId(email, masinaId);
	}
	
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	public RatingDTO deleteById(@PathVariable("id") int id) {
		return ratingService.deleteById(id);
	}
	
	@RequestMapping(value = "/update/nota/{id}", method = RequestMethod.PUT)
	public RatingDTO updateNotaById(@PathVariable("id") int id,@RequestParam(value="nota")float nota) {
		return ratingService.updateNotaById(id, nota);
	}
	
	@RequestMapping(value = "/update/comentariu/{id}", method = RequestMethod.PUT)
	public RatingDTO updateComentariuById(@PathVariable("id") int id,@RequestParam(value="comentariu")String comentariu) {
		return ratingService.updateComentariuById(id, comentariu);
	}
}
