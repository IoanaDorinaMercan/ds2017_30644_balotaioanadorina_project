package rentacar.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import rentacar.dto.UtilizatorDTO;
import rentacar.services.UtilizatorService;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/utilizator")
@Transactional
public class UtilizatorController {

	@Autowired
	private UtilizatorService utilizatorService;
	
	
	/*@RequestMapping(value = "/add", method = RequestMethod.POST)
	public int addUtilizator(@RequestBody UtilizatorDTO utilizatorDTO) {
		return utilizatorService.create(utilizatorDTO);
	}*/
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public int addUtilizator(@RequestParam("email") String email,@RequestParam("password") String password) {
		return utilizatorService.create(email,password);
	}
	
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public UtilizatorDTO loginUtilizatori(@RequestParam("email") String email,@RequestParam("password") String password) {
		System.out.println(email+" "+password);
		return utilizatorService.login(email, password);
	}
	
	@RequestMapping(value = "/find/all", method = RequestMethod.GET)
	public List<UtilizatorDTO> findAll() {
		return utilizatorService.findAll();
	}
	@RequestMapping(value = "/find/id/{id}", method = RequestMethod.GET)
	public UtilizatorDTO findUtilizatorById(@PathVariable("id") int id) {
		return utilizatorService.findById(id);
	}
	
	@RequestMapping(value = "/find/email", method = RequestMethod.GET)
	public UtilizatorDTO findUtilizatorByEmail(@RequestParam("email") String email) {
		return utilizatorService.findByEmail(email);
	}
	
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	public UtilizatorDTO deleteUtilizatorById(@PathVariable("id") int id) {
		return utilizatorService.deleteById(id);
	}
	
	@RequestMapping(value = "/delete", method = RequestMethod.DELETE)
	public UtilizatorDTO deleteUtilizatorByEmail(@RequestParam("email") String email) {
		return utilizatorService.deleteByEmail(email);
	}
	
	@RequestMapping(value = "/update/id//{id}", method = RequestMethod.PUT)
	public UtilizatorDTO updateParolaById(@PathVariable("id") int id,@RequestParam(value="parola") String parola) {
		return utilizatorService.updateParolaById(id, parola);
	}
	
	@RequestMapping(value = "/update/email", method = RequestMethod.PUT)
	public UtilizatorDTO updateParolaByEmail(@RequestParam("email") String email,@RequestParam(value="parola") String parola) {
		return utilizatorService.updateParolaByEmail(email, parola);
	}
	
}
