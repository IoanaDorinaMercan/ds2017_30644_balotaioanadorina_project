package rentacar.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import rentacar.dto.InchiriereMasinaDTO;
import rentacar.services.InchiriereMasinaService;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/inchiriere")
public class InchiriereMasinaController {

	@Autowired
	private InchiriereMasinaService inchiriereMasinaService;
	
	
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public int add(@RequestBody InchiriereMasinaDTO inchiriereMasinaDTO) {
		return inchiriereMasinaService.create(inchiriereMasinaDTO);
	}
	
	@RequestMapping(value = "/find/{id}", method = RequestMethod.GET)
	public InchiriereMasinaDTO findById(@PathVariable("id") int id) {
		return inchiriereMasinaService.findById(id);
	}
	
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	public InchiriereMasinaDTO deleteById(@PathVariable("id") int id) {
		return inchiriereMasinaService.deleteById(id);
	}
	
}
