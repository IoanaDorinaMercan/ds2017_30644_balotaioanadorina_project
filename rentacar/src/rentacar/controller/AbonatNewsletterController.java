package rentacar.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import rentacar.dto.AbonatiNewsletterDTO;
import rentacar.services.AbonatiNewsletterService;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/abonat")
public class AbonatNewsletterController {

	@Autowired
	private AbonatiNewsletterService abonatiNewsletterService;
	
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public int addAbonat(@RequestBody AbonatiNewsletterDTO abonatiNewsletterDTO)
	{
		return abonatiNewsletterService.create(abonatiNewsletterDTO);
	}
	
	@RequestMapping(value = "/delete", method = RequestMethod.DELETE)
	public AbonatiNewsletterDTO deleteAbonatByEmail(@RequestParam("email") String email) {
		return abonatiNewsletterService.deleteByEmail(email);
	}
}
