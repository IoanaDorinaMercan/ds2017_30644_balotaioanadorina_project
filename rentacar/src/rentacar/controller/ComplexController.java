package rentacar.controller;

import java.io.IOException;
import java.sql.Timestamp;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import rentacar.services.ComplexService;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/complex")
public class ComplexController {

	@Autowired
	private ComplexService complexService;
	
	
	@RequestMapping(value = "/findDistance", method = RequestMethod.GET)
	public Double findDistance(@RequestParam("sursa") String source,@RequestParam("destinatie") String destination) throws IOException
	{
		return complexService.calculateDistanceBetweenTwoLocations(source, destination);
	}
	
	@RequestMapping(value = "/findNearestCar", method = RequestMethod.GET)
	public String findNearestCar(@RequestParam("sursa") String source,@RequestParam("dataInceput") Timestamp dataInceput,@RequestParam("dataSfarsit") Timestamp dataSfarsit) throws IOException
	{
		//TODO: return ResponseEntity
		return complexService.calculateNearestAvailableCar(source, dataInceput, dataSfarsit).toString();
	}
	
}
