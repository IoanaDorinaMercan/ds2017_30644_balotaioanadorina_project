package rentacar.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import rentacar.dto.IstoricMesajeDTO;
import rentacar.services.IstoricMesajeService;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/istoricMesaje")
public class IstoricMesajeController {

	@Autowired
	private IstoricMesajeService istoricMesajeService;
	
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public int add(@RequestBody IstoricMesajeDTO istoricMesajeDTO) {
		return istoricMesajeService.create(istoricMesajeDTO);
	}
	
	@RequestMapping(value = "/find/all", method = RequestMethod.GET)
	public List<IstoricMesajeDTO> findAll() {
		return istoricMesajeService.findAll();
	}
	@RequestMapping(value = "/find/{id}", method = RequestMethod.GET)
	public IstoricMesajeDTO findById(@PathVariable("id")int id) {
		return istoricMesajeService.findById(id);
	}
	
	
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	public IstoricMesajeDTO deleetById(@PathVariable("id")int id) {
		return istoricMesajeService.deleteById(id);
	}
	
}
