package rentacar.dto;

public class AbonatiNewsletterDTO {

	
	private Integer id;
	private String nume;
	private String email;
	
	public AbonatiNewsletterDTO() {}
	
	public AbonatiNewsletterDTO(Integer id, String nume, String email) {
		super();
		this.id = id;
		this.nume = nume;
		this.email = email;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNume() {
		return nume;
	}

	public void setNume(String nume) {
		this.nume = nume;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public static class Builder{
		
		private Integer nestedid;
		private String nestednume;
		private String nestedemail;
		
		public Builder id(int id) {
			this.nestedid = id;
			return this;
		}

		public Builder nume(String nume) {
			this.nestednume = nume;
			return this;
		}
		

		public Builder email(String email) {
			this.nestedemail = email;
			return this;
		}
		
		public AbonatiNewsletterDTO create() {
			return new AbonatiNewsletterDTO(nestedid,nestednume,nestedemail);
		}
	}
	
}
