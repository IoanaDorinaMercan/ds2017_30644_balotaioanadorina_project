package rentacar.dto;

public class ClientDTO {

	private Integer id;
	private String nume;
	private String prenume;
	private String cnp;
	private String adresa;
	private Integer varsta;
	private String email;
	private String telefon;
	private String categoriePermis;
	private String vechimePermis;
	private String observatii;
	
	public ClientDTO() {}

	public ClientDTO(Integer id, String nume, String prenme, String cnp, String adresa, Integer varsta, String email,
			String telefon, String categoriePermis, String vechimePermis, String observatii) {
		super();
		this.id = id;
		this.nume = nume;
		this.prenume = prenme;
		this.cnp = cnp;
		this.adresa = adresa;
		this.varsta = varsta;
		this.email = email;
		this.telefon = telefon;
		this.categoriePermis = categoriePermis;
		this.vechimePermis = vechimePermis;
		this.observatii = observatii;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNume() {
		return nume;
	}

	public void setNume(String nume) {
		this.nume = nume;
	}

	public String getPrenume() {
		return prenume;
	}

	public void setPrenume(String prenume) {
		this.prenume = prenume;
	}

	public String getCnp() {
		return cnp;
	}

	public void setCnp(String cnp) {
		this.cnp = cnp;
	}

	public String getAdresa() {
		return adresa;
	}

	public void setAdresa(String adresa) {
		this.adresa = adresa;
	}

	public Integer getVarsta() {
		return varsta;
	}

	public void setVarsta(Integer varsta) {
		this.varsta = varsta;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelefon() {
		return telefon;
	}

	public void setTelefon(String telefon) {
		this.telefon = telefon;
	}

	public String getCategoriePermis() {
		return categoriePermis;
	}

	public void setCategoriePermis(String categoriePermis) {
		this.categoriePermis = categoriePermis;
	}

	public String getVechimePermis() {
		return vechimePermis;
	}

	public void setVechimePermis(String vechimePermis) {
		this.vechimePermis = vechimePermis;
	}

	public String getObservatii() {
		return observatii;
	}

	public void setObservatii(String observatii) {
		this.observatii = observatii;
	}

	public static class Builder
	{
		private Integer nestedid;
		private String nestednume;
		private String nestedprenume;
		private String nestedcnp;
		private String nestedadresa;
		private Integer nestedvarsta;
		private String nestedemail;
		private String nestedtelefon;
		private String nestedcategoriePermis;
		private String nestedvechimePermis;
		private String nestedobservatii;
		
		public Builder id(int id)
		{
			this.nestedid=id;
			return this;
		}
		
		public Builder adresa(String adresa)
		{
			this.nestedadresa=adresa;
			return this;
		}
		
		public Builder nume(String nume)
		{
			this.nestednume=nume;
			return this;
		}
		
		public Builder prenume(String prenume)
		{
			this.nestedprenume=prenume;
			return this;
		}
		
		public Builder cnp(String cnp)
		{
			this.nestedcnp=cnp;
			return this;
		}
		
		public Builder varsta(int varsta)
		{
			this.nestedvarsta=varsta;
			return this;
		}
		
		public Builder email(String email)
		{
			this.nestedemail=email;
			return this;
		}
		
		public Builder telefon(String telefon)
		{
			this.nestedtelefon=telefon;
			return this;
		}
		
		public Builder categoriePermis(String categoriePermis)
		{
			this.nestedcategoriePermis=categoriePermis;
			return this;
		}
		
		public Builder vechimePermis(String vechimePermis)
		{
			this.nestedvechimePermis=vechimePermis;
			return this;
		}
		
		public Builder observatii(String observatii)
		{
			this.nestedobservatii=observatii;
			return this;
		}
		
		public ClientDTO create() {
			return new ClientDTO(nestedid, nestednume,nestedprenume, nestedcnp,nestedadresa,
				nestedvarsta, nestedemail, nestedtelefon, nestedcategoriePermis,
				nestedvechimePermis,nestedobservatii);
		}

	
	}
}
