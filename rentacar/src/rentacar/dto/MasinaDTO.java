package rentacar.dto;

public class MasinaDTO {

	private Integer id;
	private String marca;
	private String model;
	private Integer anFabricatie;
	private String tipCombustibil;
	private Float consumMediu;
	private Integer caiPutere;
	private String nrInmatriculare;
	private Integer nrLocuri;
	private Float pretInchiriere;
	private String statusMasina;
	private Integer timpMinimInchiriere;

	public MasinaDTO() {
	}

	public MasinaDTO(Integer id, String marca, String model, Integer anFabricatie, String tipCombustibil,
			Float consumMediu, Integer caiPutere, String nrInmatriculare, Integer nrLocuri, Float pretInchiriere,
			String statusMasina, Integer timpMinimInchiriere) {
		super();
		this.id = id;
		this.marca = marca;
		this.model = model;
		this.anFabricatie = anFabricatie;
		this.tipCombustibil = tipCombustibil;
		this.consumMediu = consumMediu;
		this.caiPutere = caiPutere;
		this.nrInmatriculare = nrInmatriculare;
		this.nrLocuri = nrLocuri;
		this.pretInchiriere = pretInchiriere;
		this.statusMasina = statusMasina;
		this.timpMinimInchiriere = timpMinimInchiriere;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public Integer getAnFabricatie() {
		return anFabricatie;
	}

	public void setAnFabricatie(Integer anFabricatie) {
		this.anFabricatie = anFabricatie;
	}

	public String getTipCombustibil() {
		return tipCombustibil;
	}

	public void setTipCombustibil(String tipCombustibil) {
		this.tipCombustibil = tipCombustibil;
	}

	public Float getConsumMediu() {
		return consumMediu;
	}

	public void setConsumMediu(Float consumMediu) {
		this.consumMediu = consumMediu;
	}

	public Integer getCaiPutere() {
		return caiPutere;
	}

	public void setCaiPutere(Integer caiPutere) {
		this.caiPutere = caiPutere;
	}

	public String getNrInmatriculare() {
		return nrInmatriculare;
	}

	public void setNrInmatriculare(String nrInmatriculare) {
		this.nrInmatriculare = nrInmatriculare;
	}

	public Integer getNrLocuri() {
		return nrLocuri;
	}

	public void setNrLocuri(Integer nrLocuri) {
		this.nrLocuri = nrLocuri;
	}

	public Float getPretInchiriere() {
		return pretInchiriere;
	}

	public void setPretInchiriere(Float pretInchiriere) {
		this.pretInchiriere = pretInchiriere;
	}

	public String getStatusMasina() {
		return statusMasina;
	}

	public void setStatusMasina(String statusMasina) {
		this.statusMasina = statusMasina;
	}

	public Integer getTimpMinimInchiriere() {
		return timpMinimInchiriere;
	}

	public void setTimpMinimInchiriere(Integer timpMinimInchiriere) {
		this.timpMinimInchiriere = timpMinimInchiriere;
	}

	public static class Builder {

		private Integer nestedid;
		private String nestedmarca;
		private String nestedmodel;
		private Integer nestedanFabricatie;
		private String nestedtipCombustibil;
		private Float nestedconsumMediu;
		private Integer nestedcaiPutere;
		private String nestednrInmatriculare;
		private Integer nestednrLocuri;
		private Float nestedpretInchiriere;
		private String nestedstatusMasina;
		private Integer nestedtimpMinimInchiriere;

		public Builder id(int id) {
			this.nestedid = id;
			return this;
		}

		public Builder marca(String marca) {
			this.nestedmarca = marca;
			return this;
		}

		public Builder model(String model) {
			this.nestedmodel = model;
			return this;
		}

		public Builder anFabricatie(int anFabricatie) {
			this.nestedanFabricatie = anFabricatie;
			return this;
		}

		public Builder tipCombustibil(String tipCombustibil) {
			this.nestedtipCombustibil = tipCombustibil;
			return this;
		}

		public Builder consumMediu(float consumMediu) {
			this.nestedconsumMediu = consumMediu;
			return this;
		}

		public Builder caiPutere(int caiPutere) {
			this.nestedcaiPutere = caiPutere;
			return this;
		}

		public Builder nrInmatriculare(String nrInmatriculare) {
			this.nestednrInmatriculare = nrInmatriculare;
			return this;
		}

		public Builder nrLocuri(int nrLocuri) {
			this.nestednrLocuri = nrLocuri;
			return this;
		}

		public Builder pretInchiriere(float pretInchiriere) {
			this.nestedpretInchiriere = pretInchiriere;
			return this;
		}

		public Builder statusMasina(String statusMasina) {
			this.nestedstatusMasina = statusMasina;
			return this;
		}

		public Builder timpMinimInchiriere(int timpMinimInchiriere) {
			this.nestedtimpMinimInchiriere = timpMinimInchiriere;
			return this;
		}

		public MasinaDTO create() {
			return new MasinaDTO(nestedid, nestedmarca, nestedmodel, nestedanFabricatie, nestedtipCombustibil,
					nestedconsumMediu, nestedcaiPutere, nestednrInmatriculare, nestednrLocuri, nestedpretInchiriere,
					nestedstatusMasina, nestedtimpMinimInchiriere);
		}
	}
}
