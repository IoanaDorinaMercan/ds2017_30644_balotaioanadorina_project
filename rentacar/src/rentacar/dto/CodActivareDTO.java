package rentacar.dto;

import java.sql.Date;

import rentacar.entities.Client;

public class CodActivareDTO {

	private Integer id;
	private Client client;
	private String cod;
	private Boolean activat;
	private Date dataActivarii;
	
	public CodActivareDTO() {}

	public CodActivareDTO(Integer id, Client client, String cod, Boolean activat, Date dataActivarii) {
		super();
		this.id = id;
		this.client = client;
		this.cod = cod;
		this.activat = activat;
		this.dataActivarii = dataActivarii;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public String getCod() {
		return cod;
	}

	public void setCod(String cod) {
		this.cod = cod;
	}

	public Boolean getActivat() {
		return activat;
	}

	public void setActivat(Boolean activat) {
		this.activat = activat;
	}

	public Date getDataActivarii() {
		return dataActivarii;
	}

	public void setDataActivarii(Date dataActivarii) {
		this.dataActivarii = dataActivarii;
	}

	public static class Builder
	{
		private Integer nestedid;
		private Client  nestedclient;
		private String  nestedcod;
		private Boolean  nestedactivat;
		private Date  nesteddataActivarii;
		
		public Builder id(int id)
		{
			this.nestedid=id;
			return this;
		}
		
		public Builder client(Client client)
		{
			this.nestedclient=client;
			return this;
		}
		
		public Builder cod(String cod)
		{
			this.nestedcod=cod;
			return this;
		}
		
		public Builder activat(Boolean activat)
		{
			this.nestedactivat=activat;
			return this;
		}
		
		public Builder dataActivarii(Date dataActivarii)
		{
			this.nesteddataActivarii=dataActivarii;
			return this;
		}
		
		public CodActivareDTO create()
		{
			return new CodActivareDTO(nestedid,nestedclient,nestedcod,nestedactivat,nesteddataActivarii);
		}
	}
}
