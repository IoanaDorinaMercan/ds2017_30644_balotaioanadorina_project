package rentacar.dto;

public class LocatieDTO {

	private Integer id;
	private String nume;
	private String judet;
	private Float latitudine;
	private Float longitudine;
	
	public LocatieDTO() {}

	public LocatieDTO(Integer id, String nume, String judet, Float latitudine, Float longitudine) {
		super();
		this.id = id;
		this.nume = nume;
		this.judet = judet;
		this.latitudine = latitudine;
		this.longitudine = longitudine;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNume() {
		return nume;
	}

	public void setNume(String nume) {
		this.nume = nume;
	}

	public String getJudet() {
		return judet;
	}

	public void setJudet(String judet) {
		this.judet = judet;
	}

	public Float getLatitudine() {
		return latitudine;
	}

	public void setLatitudine(Float latitudine) {
		this.latitudine = latitudine;
	}

	public Float getLongitudine() {
		return longitudine;
	}

	public void setLongitudine(Float longitudine) {
		this.longitudine = longitudine;
	}

	public static class Builder{
		private Integer nestedid;
		private String nestednume;
		private String nestedjudet;
		private Float nestedlatitudine;
		private Float nestedlongitudine;
		
		public Builder id(int id) {
			this.nestedid=id;
			return this;
		}
		
		public Builder nume(String nume) {
			this.nestednume=nume;
			return this;
		}
		
		public Builder judet(String judet) {
			this.nestedjudet= judet;
			return this;
		}
		
		public Builder latitudine(float latitudine) {
			this.nestedlatitudine= latitudine;
			return this;
		}
		public Builder longitudine(float longitudine) {
			this.nestedlongitudine= longitudine;
			return this;
		}
		
		public LocatieDTO create()
		{
			return new LocatieDTO(nestedid,nestednume,nestedjudet,nestedlatitudine,nestedlongitudine);
		}
	}
}
