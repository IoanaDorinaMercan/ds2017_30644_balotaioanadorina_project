package rentacar.dto;

import rentacar.entities.Client;
import rentacar.entities.Masina;

public class RatingDTO {

	private Integer id;
	private Masina masina;
	private Client client;
	private Float nota;
	private String comentariu;
	
	public RatingDTO() {}

	public RatingDTO(Integer id, Masina masina, Client client, Float nota, String comentariu) {
		super();
		this.id = id;
		this.masina = masina;
		this.client = client;
		this.nota = nota;
		this.comentariu = comentariu;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Masina getMasina() {
		return masina;
	}

	public void setMasina(Masina masina) {
		this.masina = masina;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public Float getNota() {
		return nota;
	}

	public void setNota(Float nota) {
		this.nota = nota;
	}

	public String getComentariu() {
		return comentariu;
	}

	public void setComentariu(String comentariu) {
		this.comentariu = comentariu;
	}

	public static class Builder{
		
		private Integer nestedid;
		private Masina nestedmasina;
		private Client nestedclient;
		private Float nestednota;
		private String nestedcomentariu;
		
		public Builder id(int id)
		{
			this.nestedid=id;
			return this;
		}
		
		public Builder masina(Masina masina)
		{
			this.nestedmasina=masina;
			return this;
		}
		
		public Builder client(Client client)
		{
			this.nestedclient=client;
			return this;
		}
		
		public Builder nota(float nota )
		{
			this.nestednota=nota;
			return this;
		}
		
		public Builder comentariu(String comentariu )
		{
			this.nestedcomentariu=comentariu;
			return this;
		}
		
		public RatingDTO create()
		{
			return new RatingDTO(nestedid,nestedmasina,nestedclient,nestednota,nestedcomentariu);
		}
	}
}
