package rentacar.dto;

import java.sql.Timestamp;

import rentacar.entities.Client;
import rentacar.entities.Locatie;
import rentacar.entities.Masina;

public class InchiriereMasinaDTO {

	private Integer id;
	private Masina masina;
	private Client client;
	private Timestamp dataStart;
	private Timestamp dataStop;
	private Locatie locatieRidicare;
	private Locatie locatiePredare;
	
	public InchiriereMasinaDTO() {}
	
	public InchiriereMasinaDTO(Integer id, Masina masina, Client client, Timestamp dataStart, Timestamp dataStop,
			Locatie locatieRidicare, Locatie locatiePredare) {
		super();
		this.id = id;
		this.masina = masina;
		this.client = client;
		this.dataStart = dataStart;
		this.dataStop = dataStop;
		this.locatieRidicare = locatieRidicare;
		this.locatiePredare = locatiePredare;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Masina getMasina() {
		return masina;
	}

	public void setMasina(Masina masina) {
		this.masina = masina;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public Timestamp getDataStart() {
		return dataStart;
	}

	public void setDataStart(Timestamp dataStart) {
		this.dataStart = dataStart;
	}

	public Timestamp getDataStop() {
		return dataStop;
	}

	public void setDataStop(Timestamp dataStop) {
		this.dataStop = dataStop;
	}

	public Locatie getLocatieRidicare() {
		return locatieRidicare;
	}

	public void setLocatieRidicare(Locatie locatieRidicare) {
		this.locatieRidicare = locatieRidicare;
	}

	public Locatie getLocatiePredare() {
		return locatiePredare;
	}

	public void setLocatiePredare(Locatie locatiePredare) {
		this.locatiePredare = locatiePredare;
	}

	public static  class Builder
	{
		private Integer nestedid;
		private Masina nestedmasina;
		private Client nestedclient;
		private Timestamp nesteddataStart;
		private Timestamp nesteddataStop;
		private Locatie nestedlocatieRidicare;
		private Locatie nestedlocatiePredare;
		
		public Builder id(int id)
		{
			this.nestedid=id;
			return this;
		}
		
		public Builder masina(Masina masina)
		{
			this.nestedmasina=masina;
			return this;
		}
		
		public Builder client(Client client)
		{
			this.nestedclient=client;
			return this;
		}
		
		public Builder dataStart(Timestamp dataStart)
		{
			this.nesteddataStart=dataStart;
			return this;
		}
		
		public Builder dataStop(Timestamp dataStop)
		{
			this.nesteddataStop=dataStop;
			return this;
		}
		
		public Builder locatieRidicare(Locatie locatieRidicare)
		{
			this.nestedlocatieRidicare=locatieRidicare;
			return this;
		}
		
		public Builder locatiePredare(Locatie locatiePredare)
		{
			this.nestedlocatiePredare=locatiePredare;
			return this;
		}
		
		public InchiriereMasinaDTO create()
		{
			return new InchiriereMasinaDTO(nestedid,nestedmasina,nestedclient,nesteddataStart,nesteddataStop,nestedlocatieRidicare,nestedlocatiePredare);
		}
		
	}
	
}
