package rentacar.dto;

public class UtilizatorDTO {

	private Integer id;
	private String email;
	private String parola;
	private String rol;
	
	public UtilizatorDTO() {}

	public UtilizatorDTO(Integer id, String email, String parola, String rol) {
		super();
		this.id = id;
		this.email = email;
		this.parola = parola;
		this.rol = rol;
	}
	
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getParola() {
		return parola;
	}

	public void setParola(String parola) {
		this.parola = parola;
	}

	public String getRol() {
		return rol;
	}

	public void setRol(String rol) {
		this.rol = rol;
	}


	public static class Builder
	{
		private Integer nestedid;
		private String nestedemail;
		private String nestedparola;
		private String nestedrol;
		
		public Builder id(int id)
		{
			this.nestedid=id;
			return this;
		}
		
		public Builder email(String email)
		{
			this.nestedemail=email;
			return this;
		}
		
		public Builder parola(String parola)
		{
			this.nestedparola=parola;
			return this;
		}
		
		public Builder rol(String rol)
		{
			this.nestedrol=rol;
			return this;
		}
		
		public UtilizatorDTO create()
		{
			return new UtilizatorDTO(nestedid,nestedemail,nestedparola,nestedrol);
		}
	}
}
