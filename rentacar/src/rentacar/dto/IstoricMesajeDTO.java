package rentacar.dto;



import java.sql.Timestamp;

import rentacar.entities.Utilizator;

public class IstoricMesajeDTO {

	private Integer id;
	private Utilizator admin;
	private String mesaj;
	private Timestamp dataTrimitere;
	private Integer nrPersoane;
	
	public IstoricMesajeDTO() {}

	public IstoricMesajeDTO(Integer id, Utilizator admin, String mesaj, Timestamp nesteddataTrimitere, Integer nrPersoane) {
		super();
		this.id = id;
		this.admin = admin;
		this.mesaj = mesaj;
		this.dataTrimitere = nesteddataTrimitere;
		this.nrPersoane = nrPersoane;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Utilizator getAdmin() {
		return admin;
	}

	public void setAdmin(Utilizator admin) {
		this.admin = admin;
	}

	public String getMesaj() {
		return mesaj;
	}

	public void setMesaj(String mesaj) {
		this.mesaj = mesaj;
	}

	public Timestamp getDataTrimitere() {
		return dataTrimitere;
	}

	public void setDataTrimitere(Timestamp dataTrimitere) {
		this.dataTrimitere = dataTrimitere;
	}

	public Integer getNrPersoane() {
		return nrPersoane;
	}

	public void setNrPersoane(Integer nrPersoane) {
		this.nrPersoane = nrPersoane;
	}

	public static class Builder{
		
		private Integer nestedid;
		private Utilizator nestedutilizator;
		private String nestedmesaj;
		private Timestamp nesteddataTrimitere;
		private Integer nestednrPersoane;
		
		public Builder id(int id)
		{
			this.nestedid=id;
			return this;
		}
		
		public Builder utilizator(Utilizator utilizator )
		{
			this.nestedutilizator=utilizator;
			return this;
		}
		
		public Builder mesaj(String mesaj )
		{
			this.nestedmesaj=mesaj;
			return this;
		}
		
		
		public Builder dataTrimitere(Timestamp dataTrimitere)
		{
			this.nesteddataTrimitere=dataTrimitere;
			return this;
		}
		
		public Builder nrPersoane(int nrPersoane)
		{
			this.nestednrPersoane=nrPersoane;
			return this;
		}
		
		public IstoricMesajeDTO create()
		{
			return new IstoricMesajeDTO(nestedid,nestedutilizator,nestedmesaj,nesteddataTrimitere,nestednrPersoane);
		}
	}
}
