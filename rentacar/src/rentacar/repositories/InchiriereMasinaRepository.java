package rentacar.repositories;

import java.sql.Time;
import java.sql.Timestamp;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import rentacar.entities.InchiriereMasina;

public interface InchiriereMasinaRepository extends JpaRepository<InchiriereMasina, Integer> {

	InchiriereMasina save(InchiriereMasina inchiriereMasina);
	
	InchiriereMasina findById(int id);
	
	/*InchiriereMasina findByMasinaIdAndDataStartBetweenAndDataStopBetween(int masinaId,Timestamp dataStart1,Timestamp dataStop1,Timestamp dataStart2,Timestamp dataStop2);
	
	List<InchiriereMasina> findByMasinaIdAndDataStartLessThanAndDataStopLessThanAndDataStartLessThan(int masinaId,Timestamp dataStart,Timestamp dataStop,Timestamp dataStop1);
	
	List<InchiriereMasina> findByMasinaIdAndDataStartGreaterThanAndDataStopGreaterThanAndDataStartLessThan(int masinaId,Timestamp dataStart,Timestamp dataStop,Timestamp dataStop1);
	
	List<InchiriereMasina> findByMasinaIdAndDataStartLessThanAndDataStopLessThanAndDataStopGreaterThan(int masinaId,Timestamp dataStart,Timestamp dataStop,Timestamp dataStart1);
	*/
	//sc2
	List<InchiriereMasina> findByMasinaIdAndDataStartLessThanAndDataStopGreaterThan(int masinaId,Timestamp dataStart,Timestamp dataStop);
	//List<InchiriereMasina> findByMasinaIdAndDataStartGreaterThanAndDataStopGreaterThanAndDataStartLessThan(int masinaId,Timestamp dataStart,Timestamp dataStop,Timestamp dataStop1);
	List<InchiriereMasina> findByMasinaIdAndDataStartLessThanAndDataStopLessThanAndDataStopGreaterThan(int masinaId,Timestamp dataStart,Timestamp dataStop,Timestamp dataStart1);
	
	List<InchiriereMasina> findByMasinaIdAndDataStartGreaterThanAndDataStopGreaterThanAndDataStartLessThan(int masinaId,Timestamp dataStart,Timestamp dataStop,Timestamp dataStop1);
	List<InchiriereMasina> deleteById(int id);
	
	List<InchiriereMasina> findByDataStartBetweenAndDataStopBetween(Timestamp dataStart1,Timestamp dataStop1,Timestamp dataStart2,Timestamp dataStop2);

	List<InchiriereMasina> findLastByMasinaIdAndDataStopLessThan(int masinaId,Timestamp dataStop);
}
