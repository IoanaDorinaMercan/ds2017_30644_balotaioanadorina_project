package rentacar.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import rentacar.entities.Rating;

public interface RatingRepository extends JpaRepository<Rating, Integer> {

	Rating save(Rating rating);
	
	Rating findByClientEmailAndMasinaId(String clientEmail,int masinaId);
	
	Rating findById(int id);
	
	
	List<Rating> deleteById(int id);
}
