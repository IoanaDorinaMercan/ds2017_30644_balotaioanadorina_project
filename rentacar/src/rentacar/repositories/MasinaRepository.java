package rentacar.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import rentacar.entities.Masina;

public interface MasinaRepository extends JpaRepository<Masina, Integer>{

	Masina save(Masina masina);
	
	Masina findById(int id);
	
	@Transactional
	List<Masina> deleteById(int id);
	
	List<Masina>findAll();
}
