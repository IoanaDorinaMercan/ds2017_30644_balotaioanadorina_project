package rentacar.repositories;

import java.util.List;

import javax.swing.event.ListSelectionEvent;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import rentacar.entities.AbonatNewsletter;


public interface AbonatNewsletterRepository extends JpaRepository<AbonatNewsletter, Integer>{

	AbonatNewsletter save(AbonatNewsletter abonatNewsletter);
	
	AbonatNewsletter findByEmail(String email);
	
	@Transactional
	List<AbonatNewsletter> deleteByEmail(String email);
	
	List<AbonatNewsletter>findAll();
}
