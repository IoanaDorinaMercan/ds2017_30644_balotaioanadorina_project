package rentacar.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import rentacar.entities.IstoricMesaje;

public interface IstoricMesajeRepository extends JpaRepository<IstoricMesaje, Integer> {

	IstoricMesaje save(IstoricMesaje istoricMesaje);
	
	IstoricMesaje findById(int id);
	
	List<IstoricMesaje> deleteById(int id);
}
