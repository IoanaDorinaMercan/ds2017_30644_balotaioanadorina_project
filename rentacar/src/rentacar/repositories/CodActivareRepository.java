package rentacar.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import rentacar.entities.CodActivare;



public interface CodActivareRepository extends JpaRepository<CodActivare, Integer> {

	CodActivare save(CodActivare codActivare);
	
	CodActivare findByCod(String cod);
	
	@Transactional
	List<CodActivare> deleteByCod(String cod);
}
