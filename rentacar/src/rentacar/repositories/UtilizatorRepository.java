package rentacar.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import rentacar.entities.Utilizator;;

public interface UtilizatorRepository extends JpaRepository<Utilizator, Integer>{

	Utilizator findById(int id);
	
	Utilizator findByEmail(String email);
	
	Utilizator findByEmailAndParola(String email,String parola);
	
	Utilizator save(Utilizator utilizator);
	
	@Transactional
	List<Utilizator> deleteById(int id);
	
	@Transactional
	List<Utilizator> deleteByEmail(String email);
}
