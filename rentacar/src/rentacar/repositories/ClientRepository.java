package rentacar.repositories;
 import rentacar.entities.*;
 import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
 
public interface ClientRepository extends JpaRepository<Client, Integer>{

	Client save(Client client);
	
	Client findByEmail(String email);
	
	Client findByTelefon(String telefon);
	
	Client findByCnp(String cnp);
	
	Client findById(int id);
	
	@Transactional
	List<Client> deleteById(int id);
	
	@Transactional
	List<Client> deleteByCnp(String cnp);
	
	@Transactional
	List<Client> deleteByEmail(String email);
	
	
}
