package rentacar.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import rentacar.entities.Locatie;

public interface LocatieRepository extends JpaRepository<Locatie, Integer>{
	
	Locatie save(Locatie locatie);
	
	Locatie findById(int id);
	
	Locatie findByNume(String nume);
	
	Locatie findByLatitudineBetweenAndLongitudineBetween(float latitudine1,float latitudine2,float longitudine1,float longitudine2);
	
	@Transactional
	List<Locatie> deleteById(int id);
	
	Locatie deleteByLatitudineAndLongitudine(float latitudine, float longitudine);

}
