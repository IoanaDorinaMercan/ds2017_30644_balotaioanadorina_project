(function() {

	var istoricMesajeModule = angular.module('istoricMesajeController', [ 'ngRoute' ])

	istoricMesajeModule.config(function($routeProvider) {
		$routeProvider.when('/istoricMesaje', {
			templateUrl : 'app/views/istoricMesaje/istoricMesaje-list.html',
			controller : 'AllIstoricMesajeController',
			controllerAs : "allIstoricMesajeCtrl"
		}).when('/istoricMesaje/:id', {
			templateUrl : 'app/views/istoricMesaje/istoricMesaje-details.html',
			controller : 'IstoricMesajeController',
			controllerAs : "istoricMesajeCtrl"
		})

	});

	istoricMesajeModule.controller('AllIstoricMesajeController', [ '$scope', 'IstoricMesajeFactory',
			function($scope,IstoricMesajeFactory) {
				$scope.istoricMesaje = [];
				var promise = IstoricMesajeFactory.findAll();
				promise.success(function(data) {
					$scope.istoricMesaje = data;
				}).error(function(data, status, header, config) {
					alert(status);
				});

			} ]);

	istoricMesajeModule.controller('IstoricMesajeController', [ '$scope', '$routeParams',
			'IstoricMesajeFactory', function($scope, $routeParams, IstoricMesajeFactory) {
				var id = $routeParams.id;
				var promise = IstoricMesajeFactory.findById(id);
				$scope.istoricMesaje = null;
				promise.success(function(data) {
					$scope.istoricMesaje = data;
				}).error(function(data, status, header, config) {
					alert(status);
				});
			} ]);


})();
