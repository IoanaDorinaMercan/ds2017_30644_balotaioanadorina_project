(function() {

	var clientModule = angular.module('clientController', [ 'ngRoute' ])

		clientModule.config(function($routeProvider) {
		$routeProvider.when('/client', {
			templateUrl : 'app/views/client/operatiiClient.html',
			controller : 'OperatiiClientController'
		}).when('/registerClient', {
			templateUrl : 'app/views/client/register.html',
			controller : 'RegisterClientController',
			controllerAs : "registerClientCtrl"
		})
		.when('/loginClient', {
			templateUrl : 'app/views/client/loginClient.html',
			controller : 'LoginClientController'
		})
		.when('/inchiriereMasina', {
			templateUrl : 'app/views/client/inchiriereMasina.html',
			controller : 'InchiriereMasinaController'
		})
		.when('/inchiriereMasina/masini/:id', {
			templateUrl : 'app/views/client/inchiriereMasina-detalii.html',
			controller : 'InchiriereMasinaDetaliiController'
		})
		.when('/inchiriereMasina/inchiriaza/:id', {
			templateUrl : 'app/views/client/adaugaInchiriere.html',
			controller : 'AdaugaInchiriereController'
		})
		.when('/operatiiClient', {
			templateUrl : 'app/views/client/functionalitatiClient.html',
			controller : 'OperatiiClientController'
		})
	});


	clientModule.controller('LoginClientController', [ '$rootScope','$scope', '$routeParams','$location',
		'ClientFactory', function($rootScope,$scope, $routeParams, $location,ClientFactory) {
	 $scope.login = function ()
	 {
		 var email=$scope.username;
		 var password=$scope.password;
		 window.alert(email);
		 $scope.login = null;
		 var promise=ClientFactory.findClientByEmail(email);
		 promise.success(function(data) {
				$scope.login = data;
				window.alert("Logarea a avut succes");
				//window.alert(data.id);
				$rootScope.globals = {
		                currentUser: {
		                    username: data.email,
		                    authdata: data.id
		                }
		            };
				window.alert($rootScope.globals.currentUser.authdata);
				//$cookieStore.put('globals', $rootScope.globals);*/
				
				$location.path("/operatiiClient");
			}).error(function(data, status, header, config) {
				window.alert("Eroare logare");
				window.alert(status);
				alert(status);
			});

	 }
		} ]);

	clientModule.controller('RegisterClientController', [ '$scope', '$routeParams',
			'ClientFactory', function($scope, $routeParams, ClientFactory) {
		
		$scope.register=function()
		{
			var newClient={
				nume:$scope.nume,
				prenume:$scope.prenume,
				cnp:$scope.cnp,
				adresa:$scope.adresa,
				varsta:$scope.varsta,
				email:$scope.email,
				telefon:$scope.telefon,
				categoriePermis:$scope.categoriePermis,
				vechimePermis:$scope.vechimePermis,
				codActivare:1,
				observatii:"fara observatii"
			}
			

		    newClient=JSON.stringify(newClient);
			
			
			var promise = ClientFactory.addClient(newClient);
			var client;
			
			promise.success(function(data) {
				window.alert("Client adaugat cu succes");
				ClientFactory.createCode(data);
				client=data;
				
				
			}).error(function(data, status, header, config) {
				window.alert("Eroare adaugare client");
				alert(status);
				
			});
			
			/*if(client>-1)
				{
				promise = ClientFactory.createCode(client);
				promise.success(function(data) {
					window.alert(data);
				}).error(function(data, status, header, config) {
					alert(status);
				});
				}*/
		}	
		
			} ]);
	
	clientModule.controller('OperatiiClientController', [ '$scope', '$routeParams',
		'ClientFactory', function($scope, $routeParams, ClientFactory) {
	
	
	
		} ]);
	
	clientModule.controller('InchiriereMasinaController', [ '$scope', 'MasinaFactory',
		function($scope,MasinaFactory) {
			$scope.masini = [];
			var promise = MasinaFactory.findAll();
			promise.success(function(data) {
				//window.alert("O noua inchiriere a fost adaugata");
				$scope.masini = data;
			}).error(function(data, status, header, config) {
				//window.alert("Eroare adaugare inchiriere");
				alert(status);
			});

		} ]);

	clientModule.controller( 'InchiriereMasinaDetaliiController', ['$scope', '$routeParams',
		'MasinaFactory','LocatieFactory', function($scope, $routeParams, MasinaFactory,LocatieFactory) {
			var id = $routeParams.id;
			var promise = MasinaFactory.findById(id);
			$scope.masini = null;
			promise.success(function(data) {
				$scope.masini = data;
			}).error(function(data, status, header, config) {
				alert(status);
			});
		} ]);
	
	
	clientModule.controller( 'OperatiiClientController', ['$scope', '$routeParams',
		'MasinaFactory','LocatieFactory', function($scope, $routeParams, MasinaFactory,LocatieFactory) {
			/*var id = $routeParams.id;
			var promise = MasinaFactory.findById(id);
			$scope.masini = null;
			promise.success(function(data) {
				$scope.masini = data;
			}).error(function(data, status, header, config) {
				alert(status);
			});*/
		} ]);
	clientModule.controller('AdaugaInchiriereController', [ '$rootScope','$scope', '$routeParams',
		'InchiriereMasinaFactory','MasinaFactory','ClientFactory','LocatieFactory', function($rootScope,$scope, $routeParams, InchiriereMasinaFactory,MasinaFactory,ClientFactory,LocatieFactory) {
	
	$scope.inchiriere=function()
	{

		var masinaId=$routeParams.id;
		//window.alert(masinaId);
		var masina=null;
		var promise = MasinaFactory.findById(masinaId);
		$scope.masina = null;
		promise.success(function(data) {
			$scope.masina = data;
			
			var promise1=null;
			promise1 = ClientFactory.findClientByEmail("dumitru@yahoo.com");
			$scope.client = null;
			promise1.success(function(data) {
				$scope.client = data;
				//window.alert("client"+data.id);
				
				
				var locatieRidicare={
						id:-1,
						nume:$scope.numeLocatieRidicare,
						judet:"BN",//$scope.judetLocatieRidicare,
						latitudine:12,//$scope.latitudineLocatieRidicare,
						longitudine:13//$scope.longitudineLocatieRidicare
					};
				
				//window.alert("ajunge1");
				
					var locatiePredare={
							id:-1,
							nume:$scope.numeLocatiePredare,
							judet:"BN",//$scope.judetLocatiePredare,
							latitudine:14,//$scope.latitudineLocatiePredare,
							longitudine:15//$scope.longitudineLocatiePredare
						};
					
					//window.alert("ajunge2");
					
					var promise2=null;
					var l1=null;
					var locatieRidicare1=JSON.stringify(locatieRidicare);
					promise2 = LocatieFactory.add(locatieRidicare1);
					promise2.success(function(data) {
						locatieRidicare.id=data;
						l1=data;
						//window.alert("ridicare"+l1);
						
						var promise3=null;
						var l2=null;
						var locatiePredare1=JSON.stringify(locatiePredare);
						promise3 = LocatieFactory.add(locatiePredare1);
						promise3.success(function(data) {
							locatiePredare.id=data;
							l2=data;
							//window.alert("predare"+l2);
						
							var inchiriere={
								masina:$scope.masina,
								client:$scope.client,
								dataStart:$scope.dataStart,
								dataStop:$scope.dataStop,
								locatieRidicare:locatieRidicare,
								locatiePredare:locatiePredare
											}
	    /*window.alert("loc rid"+locatieRidicare.id);
		window.alert("data start"+inchiriere.dataStart);
		window.alert("LOCATIE RIDICARE"+inchiriere.locatieRidicare.id);*/
	    inchiriere=JSON.stringify(inchiriere);
		var promise4=null;
		promise4 = InchiriereMasinaFactory.addInchiriere(inchiriere);
		promise4.success(function(data) {
			window.alert("id inchiriere"+data);
			if(data==-1)
				window.alert("Nu se poate inchiria in perioada respectiva")
			else window.alert("Inchiriere adaugata cu succes");
		})})
		.error(function(data, status, header, config) {
			window.alert("Eroare adaugare inchiriere");
			alert(status);
		});
							
					}).error(function(data, status, header, config) {
						alert(status);
					});
				
			}).error(function(data, status, header, config) {
				alert(status);
			});
		}).error(function(data, status, header, config) {
			alert(status);
		});
		/*promise.success(function(data) {
			$scope.masina = data;
			masina=data;
			window.alert("masina"+data.id);
			window.alert("masina"+$scope.masina.id);
		}).error(function(data, status, header, config) {
			alert(status);
		});
		
		window.alert("masinagfgf"+masina.id);
		
		promise=null;
		promise = ClientFactory.findClientByEmail("dumitru@yahoo.com");
		$scope.client = null;
		promise.success(function(data) {
			$scope.client = data;
		}).error(function(data, status, header, config) {
			alert(status);
		});
		window.alert("client"+$scope.client.id);*/
		
		/*var locatieRidicare={
			id:-1,
			nume:$scope.numeLocatieRidicare,
			judet:$scope.judetLocatieRidicare,
			latitudine:$scope.latitudineLocatieRidicare,
			longitudine:$scope.longitudineLocatieRidicare
		};
		
		var locatiePredare={
				id:-1,
				nume:$scope.numeLocatiePredare,
				judet:$scope.judetLocatiePredare,
				latitudine:$scope.latitudineLocatiePredare,
				longitudine:$scope.longitudineLocatiePredare
			};
		
		promise=null;
		promise = LocatieFactory.add(locatieRidicare);
		promise.success(function(data) {
			locatieRidicare.id=data.id;
		}).error(function(data, status, header, config) {
			alert(status);
		});
		window.alert("ridicare"+locatieRidicare.id);
		
		promise=null;
		promise = LocatieFactory.add(locatiePredare);
		promise.success(function(data) {
			locatiePredare.id=data.id;
		}).error(function(data, status, header, config) {
			alert(status);
		});
		
		window.alert("predare"+locatiePredare.id);
		var inchiriere={
			masina:$scope.masina,
			client:$scope.client,
			dataStart:$scope.dataStart,
			dataStop:$scope.dataStop,
			locatieRidicare:locatieRidicare,
			locatiePredare:LocatiePredare
		}
		
		window.alert("data start"+inchiriere.dataStart);

	    inchiriere=JSON.stringify(inchiriere);
		
		promise=null;
		promise = InchiriereMasinaFactory.addInchiriereMasina(inchiriere);
		promise.success(function(data) {
			window.alert("id inchiriere"+data.id);
		}).error(function(data, status, header, config) {
			alert(status);
		});
		*/
	}	
	
		} ]);
})();
