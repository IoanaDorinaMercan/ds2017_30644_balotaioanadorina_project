(function() {

	var homeModule = angular.module('navControllers', [ 'ngRoute' ])

	homeModule.config(function($routeProvider) {
		$routeProvider.when('/', {
			templateUrl : 'app/views/homeview.html',
			controller : 'HomeController',
			controllerAs : "homeCtrl"
		}).when('/masini/:id', {
			templateUrl : 'app/views/masina/masini-details.html',
			controller : 'DetaliiMasinaController'
		})
	});

	homeModule.controller('HomeController', [ '$scope', 'MasinaFactory',
		function($scope,MasinaFactory) {
			$scope.masini = [];
			var promise = MasinaFactory.findAll();
			promise.success(function(data) {
				$scope.masini = data;
			}).error(function(data, status, header, config) {
				alert(status);
			});

		} ]);

	homeModule.controller( 'DetaliiMasinaController', [ '$scope', '$routeParams',
		'MasinaFactory', function($scope, $routeParams, MasinaFactory) {
			var id = $routeParams.id;
			var promise = MasinaFactory.findById(id);
			$scope.masini = null;
			promise.success(function(data) {
				$scope.masini = data;
			}).error(function(data, status, header, config) {
				alert(status);
			});
		} ]);
})();