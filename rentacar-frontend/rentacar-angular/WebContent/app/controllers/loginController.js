(function() {

	var loginModule = angular.module('loginController', [ 'ngRoute' ])

	loginModule.config(function($routeProvider) {
		$routeProvider.when('/login', {
			templateUrl : 'app/views/login/login.html',
			controller : 'LoginController',
			controllerAs : "loginCtrl"
		})

	});



	loginModule.controller('LoginController', [ '$scope', '$routeParams','$location',
			'UtilizatoriFactory', function($scope, $routeParams, $location,UtilizatoriFactory) {
		 $scope.login = function ()
		 {//doar pt admin in acest moment
			 var email=$scope.username;
			 var password=$scope.password;
			 window.alert(email);
			 $scope.login = null;
			 var promise=UtilizatoriFactory.login(email,password);
			 promise.success(function(data) {
					$scope.login = data;
					window.alert(data.id);
				}).error(function(data, status, header, config) {
					window.alert(status);
					alert(status);
				});

		 }
			} ]);

})();
