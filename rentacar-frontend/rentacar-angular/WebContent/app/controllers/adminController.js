(function() {

	var adminModule = angular.module('adminController', [ 'ngRoute' ])

	adminModule.config(function($routeProvider) {
		$routeProvider.when('/admin', {
			templateUrl : 'app/views/admin/loginAdmin.html',
			controller : 'LoginAdminController'
			
		}).when('/adaugareAdmin', {
			templateUrl : 'app/views/admin/adaugareAdmin.html',
			controller : 'AdaugareAdminController'
			
		})
		.when('/operatiiAdmin', {
			templateUrl : 'app/views/admin/operatiiAdmin.html',
			controller : 'OperatiiAdminController'
			
		})
		.when('/stergereAdmin', {
			templateUrl : 'app/views/admin/stergereAdmin.html',
			controller : 'StergereAdminController'
			
		})
		.when('/stergeAdmin/:id', {
			templateUrl : 'app/views/admin/stergereAdmin.html',
			controller : 'StergereUnAdminController'
			
		})
		.when('/adaugareMasina', {
			templateUrl : 'app/views/admin/adaugareMasina.html',
			controller : 'AdaugareMasinaController'
			
		});

	});

	adminModule.controller('LoginAdminController', [ '$rootScope','$scope', '$routeParams','$location',
		'UtilizatoriFactory', function($rootScope,$scope, $routeParams, $location,UtilizatoriFactory) {
	 $scope.login = function ()
	 {
		 var email=$scope.username;
		 var password=$scope.password;
		 window.alert(email);
		 $scope.login = null;
		 var promise=UtilizatoriFactory.login(email,password);
		 promise.success(function(data) {
				$scope.login = data;
				window.alert(data.id);
				$rootScope.globals = {
		                currentUser: {
		                    username: data.email,
		                    authdata: data.id
		                }
		            };
				window.alert($rootScope.globals.currentUser.authdata);
				//$cookieStore.put('globals', $rootScope.globals);*/
				
				//window.location('app/views/admin/operatiiAdmin.html');
				$location.path("/operatiiAdmin");
			}).error(function(data, status, header, config) {
				window.alert(status);
				alert(status);
			});

	 }
		} ]);
	
	
	adminModule.controller('AdaugareAdminController', [ '$scope', '$routeParams','$location',
		'UtilizatoriFactory', function($scope, $routeParams, $location,UtilizatoriFactory) {
	 $scope.register = function ()
	 {
		 var email=$scope.username;
		 var password=$scope.password;
		 window.alert(email);
		 $scope.register = null;
		 var promise=UtilizatoriFactory.add(email,password);
		 promise.success(function(data) {
				$scope.register = data;
				window.alert(data);
//				window.location('app/views/admin/operatiiAdmin.html');
				$location.path("/operatiiAdmin");
			}).error(function(data, status, header, config) {
				alert(status);
			});

	 }
		} ]);
	
	
	adminModule.controller('OperatiiAdminController', [ '$scope', '$routeParams','$location',
		'UtilizatoriFactory', function($scope, $routeParams, $location,UtilizatoriFactory) {
	/* $scope.register = function ()
	 {
		 var email=$scope.username;
		 var password=$scope.password;
		 window.alert(email);
		 $scope.register = null;
		 var promise=UtilizatoriFactory.add(email,password);
		 promise.success(function(data) {
				$scope.register = data;
				window.alert(data);
//				window.location('app/views/admin/operatiiAdmin.html');
				$location.path('/adaugareAdmin');
			}).error(function(data, status, header, config) {
				alert(status);
			});

	 }*/
		} ]);
	
	adminModule.controller('StergereAdminController', [ '$scope', '$routeParams','$location',
		'UtilizatoriFactory', function($scope, $routeParams, $location,UtilizatoriFactory) {
		$scope.admin = [];
		var promise = UtilizatoriFactory.findAll();
		promise.success(function(data) {
			$scope.admin = data;
		}).error(function(data, status, header, config) {
			alert(status);
		});
		} ]);

	adminModule.controller('StergereUnAdminController', [ '$scope', '$routeParams','$location',
		'UtilizatoriFactory', function($scope, $routeParams, $location,UtilizatoriFactory) {
		var id=$routeParams.id;
		var promise = UtilizatoriFactory.deleteById(id);
		promise.success(function(data) {

			$location.path("/stergereAdmin")
		}).error(function(data, status, header, config) {
			alert(status);
		});
		} ]);
	
	adminModule.controller('AdaugareMasinaController', [ '$scope', '$routeParams','$location',
		'UtilizatoriFactory','MasinaFactory', function($scope, $routeParams, $location,UtilizatoriFactory,MasinaFactory) {
		
		$scope.adaugaMasina=function(){
			
			var an=$scope.anFabricatie;
			window.alert($scope.marca);
			window.alert(an);
		var masina={
			 marca:$scope.marca,
			 model:$scope.model,
			 anFabricatie:$scope.anFabricatie,
			 tipCombustibil:$scope.tipCombustibil,
			 consumMediu:$scope.consumMediu,
			 caiPutere:$scope.caiPutere,
			 nrInmatriculare:$scope.nrInmatriculare,
			 nrLocuri:$scope.nrLocuri,
			 pretInchiriere:$scope.pretInchiriere,
			 statusMasina:$scope.statusMasina,
			 timpMinimInchiriere:$scope.timpMinimInchiriere
		};
		
		masina=JSON.stringify(masina);
		
		var promise = MasinaFactory.add(masina);
		promise.success(function(data) {
			window.alert("succes");
			$location.path("/operatiiAdmin");
		}).error(function(data, status, header, config) {
			alert(status);
		});
		} }]);

})();
