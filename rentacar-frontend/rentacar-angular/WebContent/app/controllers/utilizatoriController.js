(function() {

	var utilizatoriModule = angular.module('utilizatoriController', [ 'ngRoute' ])

	utilizatoriModule.config(function($routeProvider) {
		$routeProvider.when('/utilizatori', {
			templateUrl : 'app/views/user/user-list.html',
			controller : 'AllUtilizatoriController',
			controllerAs : "allutilizatoriCtrl"
		}).when('/utilizatori/:id', {
			templateUrl : 'app/views/user/user-details.html',
			controller : 'UtilizatoriController',
			controllerAs : "utilizatoriCtrl"
		})

	});

	utilizatoriModule.controller('AllUtilizatoriController', [ '$scope', 'UtilizatoriFactory',
			function($scope, UtilizatoriFactory) {
				$scope.utilizatori = [];
				var promise = UtilizatoriFactory.findAll();
				promise.success(function(data) {
					$scope.utilizatori = data;
				}).error(function(data, status, header, config) {
					alert(status);
				});

			} ]);

	utilizatoriModule.controller('UtilizatoriController', [ '$scope', '$routeParams',
			'UtilizatoriFactory', function($scope, $routeParams, UtilizatoriFactory) {
				var id = $routeParams.id;
				var promise = UtilizatoriFactory.findById(id);
				$scope.utilizatori = null;
				promise.success(function(data) {
					$scope.utilizatori = data;
				}).error(function(data, status, header, config) {
					alert(status);
				});
			} ]);

	/*var gems = [ {
		id : 1,
		name : 'My name1',
		address : "address 1",
		occupation : 'Something1'
	}, {
		id : 2,
		name : 'My name2',
		address : "address 2",
		occupation : 'Something2'
	} ];*/

})();
