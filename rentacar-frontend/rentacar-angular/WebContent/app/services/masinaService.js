(function() {
	var masinaService = angular.module('masinaService', []);

	masinaService.factory('MasinaFactory', [ '$http', 'config',
			function($http, config) {

				var masinaDetails = function(id) {
					return $http.get(config.API_URL + '/masina/find/' + id);
				};

				var masinaList = function() {
					return $http.get(config.API_URL + '/masina/find/all');
				};
				
				var addMasina = function(newMasina) {
					return $http.post(config.API_URL + '/masina/add',newMasina);
				};

				return {
					findById : function(id) {
						return masinaDetails(id);
					},

					findAll : function() {
						return masinaList();
					},
					add : function(newMasina) {
						return addMasina(newMasina);
					}
				};
			} ]);

})();