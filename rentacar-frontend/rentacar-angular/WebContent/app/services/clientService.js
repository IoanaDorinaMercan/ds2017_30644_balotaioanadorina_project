(function() {
	var clientService = angular.module('clientService', []);

	clientService.factory('ClientFactory', [ '$http', 'config',
			function($http, config) {

				var findClientByEmail = function(email) {
					return $http.get(config.API_URL + '/client/findByEmail?', {params:{email:email}});
				};

				var findClientByCnp = function(cnp) {
					return $http.get(config.API_URL + '/client/findByCnp', {params:{cnp:cnp}});
				};

				var addClient = function(newClient) {
					return $http.post(config.API_URL + '/client/add', newClient);
				};
				var createActivationCode=function(id)
				{
					return $http.post(config.API_URL + '/cod/add?', {params:{clientId:id}});
				};
				
				return {
					findClientByEmail : function(email) {
						return findClientByEmail(email);
					},

					findClientByCnp : function(cnp) {
						return findClientByCnp(cnp);
					},
					addClient: function(newClient)
					{
						return addClient(newClient);
					},
					createCode:function(id)
					{
						return createActivationCode(id);
					}
				};
			} ]);

})();