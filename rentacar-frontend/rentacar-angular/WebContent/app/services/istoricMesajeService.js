(function() {
	var istoricMesajeService = angular.module('istoricMesajeService', []);

	istoricMesajeService.factory('IstoricMesajeFactory', [ '$http', 'config',
			function($http, config) {

				var privateIstoricMesajeDetails = function(id) {
					return $http.get(config.API_URL + '/istoricMesaje/find/' + id);
				};

				var privateIstoricMesajeList = function() {
					return $http.get(config.API_URL + '/istoricMesaje/find/all');
				};

				return {
					findById : function(id) {
						return privateIstoricMesajeDetails(id);
					},

					findAll : function() {
						return privateIstoricMesajeList();
					}
				};
			} ]);

})();