(function() {
	var inchiriereMasinaService = angular.module('inchiriereMasinaService', []);

	inchiriereMasinaService.factory('InchiriereMasinaFactory', [ '$http', 'config',
			function($http, config) {

				var addInchiriereMasina = function(newInchiriereMasina) {
					return $http.post(config.API_URL + '/inchiriere/add',newInchiriereMasina);
				};

				var findInchiriereMasina = function(id) {
					return $http.get(config.API_URL + '/inchiriereMasina/find/'+id);
				};

				var findCeaMaiApropiata = function(sursa,dataInceput,dataSfarsit) {
					return $http.get(config.API_URL + '/complex/findNearestCar?',{params:{sursa:sursa,dataInceput:dataInceput,dataSfarsit:dataSfarsit}});
				};
				return {
					findById : function(id) {
						return findInchiriereMasina(id);
					},
					
					findNearest: function(sursa,dataInceput,dataSfarsit){
						return findCeaMaiApropiata(sursa,dataInceput,dataSfarsit);
					},

					addInchiriere : function(newInchiriereMasina) {
						return addInchiriereMasina(newInchiriereMasina);
					}
				};
			} ]);

})();