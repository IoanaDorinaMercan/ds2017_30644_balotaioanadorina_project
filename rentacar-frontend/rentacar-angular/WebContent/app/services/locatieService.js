(function() {
	var locatieService = angular.module('locatieService', []);

	locatieService.factory('LocatieFactory', [ '$http', 'config',
			function($http, config) {

				var addLocatie = function(newLocatie) {
					return $http.post(config.API_URL + '/locatie/add',newLocatie);
				};

				return {
					add : function(newLocatie) {
						return addLocatie(newLocatie);
					}
				};
			} ]);

})();