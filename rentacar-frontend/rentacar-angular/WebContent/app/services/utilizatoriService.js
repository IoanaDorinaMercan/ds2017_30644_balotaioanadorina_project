(function() {
	var utilizatoriService = angular.module('utilizatoriService', []);

	utilizatoriService.factory('UtilizatoriFactory', [ '$http', 'config',
			function($http, config) {

				var privateUtilizatoriDetails = function(id) {
					return $http.get(config.API_URL + '/utilizator/find/id/' + id);
				};

				var privateUtilizatoriList = function() {
					return $http.get(config.API_URL + '/utilizator/find/all');
				};

				var privateFindUtilizatoriByEmail = function(username) {
					return $http.get(config.API_URL + '/utilizator/find/email?',{params:{email:username}});
				};
				
				var privateAddUtilizatori = function(email,parola) {
					return $http.post(config.API_URL + '/utilizator/add?email='+email+'&password='+parola);//{data:{email:email,password:parola}});
				};
				
				var privateLoginUtilozator = function(email,parola) {
					return $http.get(config.API_URL + '/utilizator/login?',{params:{email:email,password:parola}});
				};
				
				var deleteById = function(id) {
					return $http.delete(config.API_URL + '/utilizator/delete/'+id);
				};
				return {
					add : function(email,parola){
						return privateAddUtilizatori(email,parola);
					},
					
					findById : function(id) {
						return privateUtilizatoriDetails(id);
					},

					findByEmail : function(username) {
						return privateFindUtilizatoriByEmail(username);
					},
					findAll : function() {
						return privateUtilizatoriList();
					},
					
					login : function(email,parola) {
						return privateLoginUtilozator(email,parola);
					},
					deleteById:function(id){
						return deleteById(id);
					}
				};
			} ]);

})();