
(function() {
	var app = angular.module('rentacar-angular', [ 'ngRoute', 'configModule',
		'navControllers', 
		'utilizatoriController',
		'utilizatoriService',
		'istoricMesajeController',
		'istoricMesajeService',
		'loginController',
		'utilizatoriService',
		'clientController',
		'clientService',
		'navControllers',
		'masinaService',
		'clientController',
		'inchiriereMasinaService',
		'clientController',
		'locatieService',
		'adminController',
		'locatieService'
		])
})();
/*aici e un fel de modul principal, trebuie controller-ul si serviciul principal
 aparent, nu merg mai multe controllere/servicii
 */