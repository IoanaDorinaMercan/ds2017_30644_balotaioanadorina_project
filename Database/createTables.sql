drop database rentacar;
create database if not exists rentacar;
use rentacar;


drop table masini;
create table masini
(id int auto_increment primary key not null,
marca varchar(50) not null,
model varchar(50) not null,
an_fabricatie int not null,
tip_combustibil varchar(15) not null,
consum_mediu float not null,
cai_putere int not null,
nr_inmatriculare varchar(10) not null,
nr_locuri int not null,
pret_inchiriere float not null,
status_masina varchar(15) not null,
timp_minim_inchiriere int not null);


create table locatii
(id int auto_increment primary key not null,
nume varchar(100) not null,
judet varchar(100) not null,
latitudine float not null,
longitudine float not null)

drop table clienti;

create table clienti
(id int auto_increment primary key not null,
nume varchar(50) not null,
prenume varchar(50) not null,
cnp varchar(13) unique not null,
adresa varchar(100) not null,
varsta int not null,
email varchar(50) unique not null,
telefon varchar(15) unique not null,
categorie_permis varchar(15) not null,
vechime_permis varchar(20) not null,
observatii varchar(100))

create table inchirieri_masini
(id int auto_increment primary key not null,
id_masina int not null,
id_client int not null,
data_start datetime not null,
data_stop datetime not null,
id_locatie_ridicare int not null,
id_locatie_predare int not null,
FOREIGN KEY (id_masina) references masini(id),
FOREIGN KEY (id_client) references clienti(id),
FOREIGN KEY (id_locatie_ridicare) references locatii(id),
FOREIGN KEY (id_locatie_predare) references locatii(id))

create table rating
(id int auto_increment primary key not null,
id_masina int not null,
id_client int not null,
nota float not null,
comentariu varchar(50) not null,
FOREIGN KEY (id_masina) references masini(id),
FOREIGN KEY (id_client) references clienti(id))

create table abonati_newsletter
(id int auto_increment primary key not null,
nume varchar(50) not null,
email varchar(50) unique not null)

create table utilizatori
(id int auto_increment primary key not null,
email varchar(50) unique not null,
parola varchar(50) not null,
rol varchar(50) not null)

drop table istoric_mesaje;
create table istoric_mesaje
(id int auto_increment primary key not null,
id_admin int,
mesaj varchar(1000) not null,
data_trimitere datetime not null,
numar_persoane int not null,
FOREIGN KEY (id_admin) references utilizatori(id))

drop table coduri_activare;
create table coduri_activare
(id int auto_increment primary key not null,
id_client int,
cod varchar(50) unique not null,
activat boolean not null,
data_activarii datetime not null,
FOREIGN KEY (id_client) references clienti(id))
